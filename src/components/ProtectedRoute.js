import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';


const ProtectedRoute = ({ isAuth, isRole, component: Component, ...rest }) => {
    return (
        <Route 
            {...rest}
            render={routeProps => {
                if(isAuth) {
                    // Additional checking for protected route to differentiate visited pages every user role
                    if(isRole === "Innovation Manager") {
                        return <Component {...routeProps} />
                    } else {
                        return (
                            <Redirect to="/InnovatorHome" />
                        )
                    }
                } else {
                    return (
                        <Redirect to="/Login" />
                    )
                }
            }}
        />
    );
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthenticated,
    isRole: localStorage.getItem("keepRole")
});

export default connect (mapStateToProps)(ProtectedRoute);