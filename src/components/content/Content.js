import React from "react";
import classNames from "classnames";
import { Container } from "reactstrap";
import { Switch, Route } from "react-router-dom";
import Home from '../../pages/HomePage';
import Team from '../../pages/TeamPage';
import Schedule from '../../pages/SchedulePage';
import Notification from '../../pages/NotificationPage';
import Setting from '../../pages/SettingPage';
import ProtectedRoute from '../ProtectedRoute';
import Topbar from "./Topbar";
import Login from '../../pages/LoginPage';
import InnovatorHome from '../../pages/InnovatorHomePage';
import InnovatorSchedule from '../../pages/InnovatorSchedulePage';
import InnovatorNotification from '../../pages/InnovatorNotificationPage';
import InnovatorSetting from '../../pages/InnovatorSettingPage';


const Content = ({ sidebarIsOpen, toggleSidebar }) => (

  <Container
    fluid
    className={classNames("content", { "is-open": sidebarIsOpen })}>
    <Topbar toggleSidebar={toggleSidebar} />
    <Switch>
      <ProtectedRoute exact path="/" component={Home} />
      <ProtectedRoute exact path="/Team" component={Team} />
      <ProtectedRoute exact path="/Schedule" component={Schedule} />
      <ProtectedRoute exact path="/Notification" component={Notification} />
      <ProtectedRoute exact path="/Setting" component={Setting} />
      <Route exact path="/Login" component={Login} />
      <Route exact path="/InnovatorHome" component={InnovatorHome} />
      <Route exact path="/InnovatorSchedule" component={InnovatorSchedule} />
      <Route exact path="/InnovatorNotification" component={InnovatorNotification} />
      <Route exact path="/InnovatorSetting" component={InnovatorSetting} />
    </Switch>
  </Container>
  
);

export default Content;
