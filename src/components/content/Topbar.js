import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import { 
  // Container, 
  Row, 
  Col, 
  Navbar, 
  Button} from "reactstrap";
import '../../App.css';
import Search from './Search';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignLeft } from "@fortawesome/free-solid-svg-icons";



const Topbar = ({ toggleSidebar }) => {

  return (
      <Row className="topbar" style={{backgroundColor: "#FFFFFF"}}>
        <Col style={{width: "25%"}}>
          <Navbar horizontal
            className="rounded"
            expand="md">
              <Button style={{backgroundColor: '#095571'}} onClick={toggleSidebar}>
              <FontAwesomeIcon icon={faAlignLeft} color="white"/>
              </Button>
          </Navbar>
        </Col>
        <Col style={{width: "100%",textAlign: "right", marginTop: "0.3%"}}>
          {/* Display email on top right corner of page */}
          <Search/>
        </Col>
      </Row>

  );
};

export default Topbar;
