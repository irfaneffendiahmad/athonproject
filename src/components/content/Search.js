import React, { useState } from 'react';
import { connect } from 'react-redux';
import { 
  Button, 
  Form, 
  Label, 
  Input, 
  ButtonDropdown, 
  DropdownToggle, 
  DropdownMenu, 
  DropdownItem,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
  Table} from 'reactstrap';
import { useHistory } from 'react-router-dom';
import { logOutAction } from '../../redux/authentication/actions';
import iconNotif2 from '../../assets/icons/iconNotif2.svg';
import iconSearch from '../../assets/icons/iconSearch.svg';
import iconTimeNotif from '../../assets/icons/iconTimeNotif.svg';
import iconRequest from '../../assets/icons/iconRequest.svg';


const SearchBar = (props) => {

    const { isLogin, logOutFunc } = props;

    const [dropdownOpen, setDropdownOpen] = useState(false);
    const toggle = () => setDropdownOpen(prevState => !prevState);

    const history = useHistory();
    const handleSignOut = () => {
      logOutFunc();
      localStorage.clear();
      history.push('/');
      window.location.reload();
  }

    return (

        // SEARCH INPUT
        <Form className="d-flex" action="/" method="get">
                <Label htmlFor="header-search">
                    <span className="visually-hidden">Search event</span>
                </Label>
                <Button 
                    type='submit'
                    style={{
                      borderStyle: 'solid none solid solid', 
                      borderColor: isLogin ? '#095571' : 'transparent', 
                      borderRadius: '15px 0 0 15px', 
                      backgroundColor: 'transparent', 
                      color: '#095571'}}>
                        {isLogin ?<img src={iconSearch}  alt="iconSearch" /> : null }
                </Button>
                <Input
                    type={isLogin ? 'text' : 'hidden'}
                    style={{
                      width: '100%', 
                      borderRadius: '0 15px 15px 0', 
                      borderStyle: 'solid solid solid none', 
                      borderColor: '#095571', 
                      backgroundColor: 'transparent'}}
                    name="s"
                    placeholder="Find your schedule" />
                  <Button 
                    type='button'
                    id= "PopoverLegacy"
                    style={{
                      borderStyle: 'none', 
                      borderColor: isLogin ? '#095571' : 'transparent', 
                      backgroundColor: 'transparent',
                      color: '#095571'}}>
                        <img src={iconNotif2}  alt="iconNotif" 
                        />
                  </Button>
                    
                  {/* NOTIFICATIONS POPOVER  */}
                  <UncontrolledPopover 
                  trigger="legacy" 
                  placement="bottom" target="PopoverLegacy"
                  style={{
                    fontFamily: "Roboto"
                  }}>
                    <PopoverHeader 
                    style={{
                      color: "white",
                      fontWeight: "bolder",
                      backgroundColor: "#095571"                 
                    }}>Notifications</PopoverHeader>
                    <PopoverBody style={{
                          borderStyle: "solid",
                          borderColor: "white white #CCCCCC white",
                          borderWidth: "1px",
                          paddingTop: "1px",
                          paddingBottom: "0px",
                          marginBottom: "-10px"
                        }}>
                          <Table borderless >
                            <tbody>
                              <tr>
                                <td><img src={iconRequest} alt="iconRequest" 
                                style={{marginLeft: "-5px"}}/></td>
                                  <tr>
                                    <td style={{
                                      fontSize: "8px",  
                                      paddingTop: "5px"}}>
                                        <img src={iconTimeNotif} alt="iconTimeNotif" /> 21 September 2021, 09.45 WIB</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><span style={{
                                    color: "#002C46",
                                    fontSize: "11px",
                                    marginTop: "-10px"
                                  }}>Team Bigbox </span>
                                  <span style={{
                                    color: "#979797",
                                    fontSize: "10px"}}>has requested to join event!</span></td></tr>
                                  
                              </tr>
                            </tbody>
                          </Table>
                      </PopoverBody>
                      <PopoverBody
                      style={{
                        color: "#979797",
                        fontSize: "11px",
                        paddingTop: "5px",
                        paddingBottom: "5px",
                        backgroundColor: "#EBEBEB",
                        textAlign: "center",
                        borderRadius: "0px 0px 3px 3px"
                      }}>
                        View all notifications
                      </PopoverBody>
                  </UncontrolledPopover>

                      {/* DROPDOWN BUTTON FOR PROFILE AND SIGNING OUT */}
                  <ButtonDropdown isOpen={dropdownOpen} toggle={isLogin ? toggle : null}>
                    <DropdownToggle split color="link" style={{
                      textDecoration: "none", 
                      color: isLogin ? "black" : '#E5E5E5', 
                      outline: 'none', 
                      borderColor: 'transparent'}}>
                      {localStorage.getItem('keepName')}
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem href="/InnovatorSetting">Profile</DropdownItem>
                      <DropdownItem onClick={handleSignOut}>Sign out</DropdownItem>
                    </DropdownMenu>
                  </ButtonDropdown>
        </Form>
    );
}

const mapStateToProps = (state) => ({
    isLogin: state.auth.isAuthenticated
  });

const mapDispatchToProps = (dispatch) => ({
    logOutFunc: () => dispatch(logOutAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);