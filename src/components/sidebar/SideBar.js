import React from "react";
import { NavItem, NavLink, Nav, Container } from "reactstrap";
import classNames from "classnames";
import { Link, useHistory } from "react-router-dom";
import logo from '../../assets/logo.svg';
import '../../App.css';
import iconEvent from '../../assets/icons/iconEvent.svg';
import iconSchedule from '../../assets/icons/iconSchedule.svg';
import iconNotif from '../../assets/icons/iconNotif.svg';
import iconSetting from '../../assets/icons/iconSetting.svg';
import iconSignOut from '../../assets/icons/iconSignOut.svg';
import iconTeam from '../../assets/icons/iconTeam.svg';


const SideBar = ({ isOpen, toggle }) => {

  let history = useHistory();

  const handleSignOut = () => {
    history.push('/');
    localStorage.clear();
    window.location.reload();
  }
  console.log('history =>', history);

  if (localStorage.getItem("keepRole") === "Innovation Manager") {

    return (
  
      <div className={classNames("sidebar", { "is-open": isOpen })}>
        <div className="sidebar-header">
          <span color="info" onClick={toggle} style={{ color: "#E5E5E5" }}>
          &times;
          </span>
          <h2>
            <Container style={{marginLeft: "-10%"}}>
            <img src={logo} className="App-logo" alt="logo" />
            </Container>
          </h2>
        </div>
        <div className="side-menu">
          <Container style={{fontSize: "18px"}}>
          <Nav vertical className="list-unstyled color-white" style={{width: "100%", height: "100%", maxHeight: "max-content"}}>
            <NavItem>
              <NavLink tag={Link} to={"/"} style={{color: '#fff'}}>
              <img src={iconEvent}  alt="iconEvent" />{' Event '}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to={"/Team"} style={{color: '#fff'}}>
              <img src={iconTeam}  alt="iconTeam" />{' Team '}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to={"/Schedule"} style={{color: '#fff'}}>
              <img src={iconSchedule}  alt="iconSchedule" />{' Schedule '}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to={"/Notification"} style={{color: '#fff'}}>
              <img src={iconNotif}  alt="iconNotif" />{' Notification '}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink tag={Link} to={"/Setting"} style={{color: '#fff'}}>
              <img src={iconSetting}  alt="iconSetting" />{' Setting '}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink onClick={() => handleSignOut()} style={{color: '#fff'}}>
              <img src={iconSignOut}  alt="iconSignOut" />{' Sign out'}
              </NavLink>
            </NavItem>
          </Nav>
          </Container>
        </div>
      </div>
    
    )
  } else {
    return (

      <div className={classNames("sidebar", { "is-open": isOpen })}>
      <div className="sidebar-header">
        <span color="info" onClick={toggle} style={{ color: "#E5E5E5" }}>
        &times;
        </span>
        <h2>
          <Container style={{marginLeft: "-10%"}}>
          <img src={logo} className="App-logo" alt="logo" />
          </Container>
        </h2>
      </div>
      <div className="side-menu">
        <Container style={{fontSize: "18px"}}>
        <Nav vertical className="list-unstyled color-white" style={{width: "100%", height: "100%", maxHeight: "max-content"}}>
          <NavItem>
            <NavLink tag={Link} to={"/InnovatorHome"} style={{color: '#fff'}}>
            <img src={iconEvent}  alt="iconEvent" />{' Event '}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to={"/InnovatorSchedule"} style={{color: '#fff'}}>
            <img src={iconSchedule}  alt="iconSchedule" />{' Schedule '}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink tag={Link} to={"/InnovatorNotification"} style={{color: '#fff'}}>
            <img src={iconNotif}  alt="iconNotif" />{' Notification '}
            </NavLink>
          </NavItem>
          <NavItem>
              <NavLink tag={Link} to={"/InnovatorSetting"} style={{color: '#fff'}}>
              <img src={iconSetting}  alt="iconSetting" />{' Setting '}
              </NavLink>
            </NavItem>
          <NavItem>
            <NavLink onClick={() => handleSignOut()} style={{color: '#fff'}}>
            <img src={iconSignOut}  alt="iconSignOut" />{' Sign out'}
            </NavLink>
          </NavItem>
        </Nav>
        </Container>
      </div>
    </div>

    )
  
  }
};

export default SideBar;