import React, { useState } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { NotificationContainer } from 'react-notifications';
import { Switch, Route } from "react-router-dom";
import SideBar from "./components/sidebar/SideBar";
import Content from "./components/content/Content";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import './App.css';
import Login from './pages/LoginPage'
import ResetPass from './pages/ResetPassPage';
import Register from './pages/RegisterPage';
import ForgotPass from './pages/ForgotPassPage';
import ForgotPassResp from './pages/ForgotPassRespPage';


const App = () => {

  const [sidebarIsOpen, setSidebarOpen] = useState(true);
  const toggleSidebar = () => setSidebarOpen(!sidebarIsOpen);

  if (localStorage.getItem("keepToken") !== null) {

    return (

      <BrowserRouter>
        <Helmet>
          {/* Display title on tab of browser */}
          <title>Sprint.in - Monev Scheduler</title>
        </Helmet>
        <div className="App wrapper">
          <SideBar toggle={toggleSidebar} isOpen={sidebarIsOpen} />
          <Content toggleSidebar={toggleSidebar} sidebarIsOpen={sidebarIsOpen} />
        </div>
      </BrowserRouter>

    );

  } else {

    return (
      <BrowserRouter>
      <Helmet>
        {/* Display title on tab of browser */}
        <title>Sprint.in - Monev Scheduler</title>
      </Helmet>
      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/ForgotPass" component={ForgotPass} />
        <Route exact path="/ForgotPassResp" component={ForgotPassResp} />
        <Route exact path="/ResetPass" component={ResetPass} />
        <Route exact path="/Register" component={Register} />
      </Switch>
      <NotificationContainer />
      </BrowserRouter>
    );

  }
};

export default App;
