import React, {useState, useEffect} from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
import { 
    Container, 
    CardBody, 
    Card, 
    Button, 
    FormGroup, 
    Form, 
    Label, 
    CustomInput,
    Input,
    // CardText, 
    Row, 
    Col, 
    Modal, 
    ModalHeader, 
    ModalBody, } from 'reactstrap';
import Select from 'react-select';
import "../assets/css/innhomepage.css";
import confirmModal from "../assets/confirmModal.svg";
import missionComplete from "../assets/missionComplete.svg"
import noEvents from "../assets/noEvents.svg";
import iconSchedule from "../assets/iconSchedule.svg";
import iconTeams from "../assets/iconTeams.svg";
import iconTeamInvited from "../assets/iconTeamInvited.svg";
import iconSchedule2 from "../assets/iconSchedule2.svg";
import iconMeetingTime from "../assets/iconMeetingTime.svg";
import axios from 'axios';
import { NotificationManager} from 'react-notifications';


const InnovatorHomePage = (props) => {

    const [isOpenModalEvent, setIsOpenModalEvent] = useState(false);
    const [isOpenSuccModalEvent, setIsOpenSuccModalEvent] = useState(false);
    const [closeAllEvent, setcloseAllEvent] = useState(false);


    // -1- Prepare to submit Monev data
    const [joinEventForm, setJoinEventForm] = useState({
        eventField: [],
        teamField: [],
        dateField: '',
        timeField: '',
        document1Field: '',
        document2Field: '',
        document3Field: '',
        document4Field: '',
    });

    const setFieldEvent = (field, value) => {
        setJoinEventForm({
            ...joinEventForm,
            [field]: value,
        })
        // console.log('Displayed Value =>', value.value);
        fetchTeamData(value.value);
    }

    const setFieldTeamEvent = (field, value) => {
        setJoinEventForm({
            ...joinEventForm,
            [field]: value,
        })
    }

    const setScheduleEvent = (field, value) => {
        setJoinEventForm({
            ...joinEventForm,
            [field]: value,
        })
    }

    const setDocEvent = (field, value) => {
        setJoinEventForm({
            ...joinEventForm,
            [field]: value,
        })
    }

    const handleCreateEvent = () => {
        setIsOpenModalEvent(true);
    }
    // -1-

        
    // -2- Grab Event data
    const [eventData, setEventData] = useState([]);
    const optEventData = eventData?.reduce((acc, data) => {
        acc.push({
            label: data.eventName,
            value: data.eventId,
        });
        return acc;
    }, []);
    
    useEffect(() => {
        const fetchEventData = async () => {
            axios({
                method: 'GET',
                    url: 'https://dev-team4-backend.herokuapp.com/api/v1/event/innovator',
                    headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                    }
            }).then((response) => {
                console.log('response for event data per id =>', response.data.data);
                setEventData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchEventData();
    }, []);
    // -2-


    // -3- Grab Team data
    const [teamData, setTeamData] = useState([]);
    const [scheduleData, setScheduleData] = useState([]);

    const fetchTeamData = (value) => {
        axios({
            method: 'GET',
            url: `https://dev-team4-backend.herokuapp.com/api/v1/event/innovator/event/${value}`,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
            setTeamData(response.data.data.teams);
            console.log('response for team data per id =>', teamData);

            setScheduleData(response.data.data.schedules);
            console.log('response for date data per id =>', scheduleData);
        }).catch(err => {
            console.log('error =>', err);
        });
    }

    const optTeamData = teamData?.reduce((acc, data) => {
        acc.push({
            label: data.name,
            value: data.id,
        });
        return acc;
    }, []);

    const optScheduleDateData = scheduleData?.reduce((acc, data) => {
        acc.push({
            label: data.date,
            value: data.date,
        });
        return acc;
    }, []);
    // -3-


    // -4- Join event submission
    const joinEvent = () => {

        // e.preventdefault();
        setIsOpenModalEvent(false);

        const formdata = new FormData();

        if(document.querySelector("#document1Field").files[0]) {
            formdata.append('document1Field',document.querySelector("#document1Field").files[0])
        }
        if(document.querySelector("#document2Field").files[0]) {
            formdata.append('document2Field',document.querySelector("#document2Field").files[0])
        }
        if(document.querySelector("#document3Field").files[0]) {
            formdata.append('document3Field',document.querySelector("#document3Field").files[0])
        }
        if(document.querySelector("#document4Field").files[0]) {
            formdata.append('document4Field',document.querySelector("#document4Field").files[0])
        }
        formdata.append("eventId",joinEventForm.eventField.value)
        formdata.append("teamId", joinEventForm.teamField.value)
        formdata.append("scheduleId", joinEventForm.timeField)

        console.log('FINAL FORM => ',formdata);

        axios({
            method: 'POST',
            url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule/select',
            data: formdata,
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
                NotificationManager.info('S U C C E S S !','',3000);
                console.log('response for join event submission =>', response.data.data);
                setJoinEventForm(response.data.data);
                window.location.reload();
        })
        .catch(err => {
            console.log('error =>', err);
            NotificationManager.error('Oops! Fail to save your data!','',3000);
        });
    }

    // console.log('joinEventForm =>', joinEventForm);
    // -4-


    // -5- Display Monev in Your Event column
    const [displayMonev, setDisplayMonev] = useState([]);

    useEffect(() => {
        const fetchDataYourEvent = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/event/innovator/event',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for your event column =>', response.data.data);
                setDisplayMonev(response.data.data);
            })
            .catch(err => {
                console.log('error =>', err);
            });
        }
        fetchDataYourEvent();
    }, []);
    // -5-


    // -6- PAGE STYLE (MODAL FUNCTION FOR CREATING EVENT)
    const {
    buttonLabel,
    className
    } = props;

    const modalToggleEvent = () => setIsOpenModalEvent(false);

    const toggleNestedEvent = () => {
        setIsOpenSuccModalEvent(false);
        setcloseAllEvent(false);
    }
    // -6-
    
    
    // -7- MODAL FUNCTION FOR DELETING EVENT//
    const [modalJoinEvent, setmodalJoinEvent] = useState(false);
    const [nestedmodalJoinEvent, setNestedmodalJoinEvent] = useState(false);
    const [closeAllJoinEvent, setcloseAllJoinEvent] = useState(false);

    const modalToggleJoinEvent = () => setmodalJoinEvent(!modalJoinEvent);
    const toggleNestedJoinEvent= () => {
        setNestedmodalJoinEvent(!nestedmodalJoinEvent);
        setcloseAllJoinEvent(false);
    }
    const toggleAllJoinEvent = () => {
        setNestedmodalJoinEvent(!nestedmodalJoinEvent);
        setcloseAllJoinEvent(true);
    }
    // -7-


    // -8- MODAL FUNCTION FOR EVENT DETAILS//
    const [modalEventDetails, setmodalEventDetails] = useState(false);
    const [indexListEventName, setIndexListEventName] = useState();

    const toggleEventDetails = (indexListEventName) => {
        setIndexListEventName(indexListEventName);
        setmodalEventDetails(!modalEventDetails);
    }
    const toggleEventDetailsX = () => setmodalEventDetails(!modalEventDetails);
    // -8-


    return (
    <Container fluid className="container-whole"> 
    
        <Row className="template">
        
        {/* THIS IS CARD FOR CREATING EVENT */}
        <Col sm={{ size: 6, offset: 0}} className="col-spacing">
            <Card >
            <CardBody className="card-body-spacings">
                <h3 className="card-headings">Choose Your Event</h3>
                <CardBody>
                    <Form>
                    <FormGroup>
                        <Label for="Event">Event</Label>
                        <Select 
                            className="input-spacing"
                            options={optEventData}
                            onChange={(selected) => setFieldEvent('eventField', selected)}
                            value={joinEventForm.eventField}
                            placeholder="Choose your event"
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="team">Team</Label>
                        <Select
                            className="input-spacing"
                            placeholder="Choose your team"
                            options={optTeamData}
                            onChange={(selected) => setFieldTeamEvent('teamField', selected)}
                            value={joinEventForm.teamField}
                        />
                    </FormGroup>
                    <Row>
                        <Col>
                        <Label for="Date">Date</Label>
                        <Select 
                                className="input-spacing"
                                options={optScheduleDateData}
                                onChange={(selected)=> setScheduleEvent('dateField', selected)}
                                placeholder="Choose your date"
                                value={joinEventForm.dateField}
                            />
                        </Col>
                        <Col>
                        <Label for="time">Time</Label>
                        {/* <Select
                                className="input-spacing"
                                options={optScheduleTimeData}
                                onChange={(selected)=> setScheduleEvent('timeField', selected)}
                                placeholder="Choose your time"
                                value={joinEventForm.timeField}
                                />  */}

                        <Input 
                        className="input-spacing"
                        type='select' 
                        name='timeField' 
                        id='timeField' 
                        onChange={(e)=> setScheduleEvent('timeField', e.target.value)} defaultValue={joinEventForm.timeField}>

                        <option selected disabled hidden>Choose your time</option>
                        {
                            scheduleData.map((data, n) => (
                                data.times.map((time, o) => (
                                    <option key={o} value={time.id}>{time.start}-{time.end}</option>
                                ))
                            ))
                        }
                        </Input>
                        </Col>
                    </Row>
                    <FormGroup>
                        <Label for="document">Document 1</Label>
                        <CustomInput
                            className="input-spacing"
                            label="Upload a Document"
                            type="file"
                            id="document1Field"
                            name="document1Field"
                            onChange={(e)=> setDocEvent('document1Field', e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="document">Document 2</Label>
                        <CustomInput
                            className="input-spacing"
                            label="Upload a Document"
                            type="file"
                            id="document2Field"
                            name="document2Field"
                            onChange={(e)=> setDocEvent('document2Field', e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="document">Document 3</Label>
                        <CustomInput
                            className="input-spacing"
                            label="Upload a Document"
                            type="file"
                            id="document3Field"
                            name="document3Field"
                            onChange={(e)=> setDocEvent('document3Field', e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label for="document">Document 4</Label>
                        <CustomInput
                            className="input-spacing"
                            label="Upload a Document"
                            type="file"
                            id="document4Field"
                            name="document4Field"
                            onChange={(e)=> setDocEvent('document4Field', e.target.value)}
                        />
                    </FormGroup>
                    </Form>
                </CardBody>
                    <Button style={{
                        backgroundColor:"#095571", 
                        float: "right", 
                        color:"#EBEBEB", 
                        borderColor:"#095571", 
                        width: "160px",
                        marginRight: "15px" }} 
                        onClick={handleCreateEvent}>Join{buttonLabel}
                    </Button>

                    <Modal isOpen={isOpenModalEvent} 
                    toggle={modalToggleEvent} 
                    className={className} 
                    style={{textAlign:"center"}}>
                    
                    <ModalHeader style={{borderBlockColor:"white"}}
                    toggle={modalToggleEvent}></ModalHeader>

                        <ModalBody>
                            <img src={confirmModal} alt='createSchedule'/>
                        <ModalBody className="card-confirm1">Documents Submission</ModalBody>
                        <ModalBody className="card-note">Are you sure to submit the documents?</ModalBody> 
                        <ModalBody style={{
                            textAlign: "center", 
                            marginBottom:"20px" }}>
                            <Row >
                            <Col className="submit-pop">
                                <Button style={{
                                    backgroundColor:"#095571", 
                                    borderColor:"#095571", 
                                    width:"100%",
                                    maxWidth:"160px", 
                                    height:"48px", 
                                    fontWeight: "bold"}}
                                    onClick={modalToggleEvent} >Cancel</Button>
                            </Col>
                            <Col>
                                <Button style={{
                                    backgroundColor:"#EA492C", 
                                    borderColor:"#EA492C",
                                    width:"100%",
                                    maxWidth:"160px", 
                                    height:"48px",
                                    fontWeight: "bold"}} 
                                    onClick={joinEvent}>Submit</Button>
                            </Col>
                            </Row>
                        </ModalBody>
                        </ModalBody>
                    </Modal>
                    <Modal isOpen={isOpenSuccModalEvent}
                    toggle={toggleNestedEvent} 
                    onClosed={closeAllEvent ? modalToggleEvent : undefined} 
                    style={{textAlign:"center"}}>
                            <ModalHeader style={{
                                borderBlockColor:"white", 
                                height:"50px"}}
                                toggle={modalToggleEvent}></ModalHeader>
                            <ModalBody style={{marginBottom:"20px"}}>
                                <img src={missionComplete} alt='congratsEvent'/>
                            <ModalBody className="card-confirm2">Congratulations!</ModalBody>
                            <ModalBody className="card-note">Your documents has been submitted.</ModalBody>
                            <Button style={{
                                backgroundColor:"#095571", 
                                borderColor:"#095571", 
                                width:"100%",
                                maxWidth:"160px",  
                                height:"48px", 
                                fontWeight: "bold"}} 
                                onClick={toggleNestedEvent}>Back to Home</Button>
                            </ModalBody>
                    </Modal>
                </CardBody>

            </Card>
        </Col>

         {/* THIS CARD TO SHOW YOUR EVENT */}
          {/* CARD WITH NO EVENTS NOTIFICATIONS*/}

         

          {/* ========================== */}
          {/* Display Your Event */}
          {/* ========================== */}


        <Col sm={{ size: 6, offset: 0}}>
            <Card>
            <CardBody className="card-body-spacing">
            <h3 className="card-headings" >Your Event</h3>

            {/* ----- Display Monev in Your Event Begin Code ----- */}
            {eventData.length > 0 ? 
            displayMonev.map((list, i) => (

                <CardBody>
                <Container className="container-event" >
                    <Row>
                    <Col key={i} sm={{ size: 8, offset: "auto" }}>
                        <Row className="monev-font" onClick={() => toggleEventDetails(list.name)}> {list.name}</Row>
                        <Modal isOpen={modalEventDetails} toggleEventDetails={toggleEventDetails} className={className} >
                            <ModalHeader toggle={toggleEventDetailsX}>
                            <h3 className="details-title1">{indexListEventName}</h3>
                            </ModalHeader>
                            <ModalBody>
                                <ModalBody className="details-title2">
                                Team Invited
                                </ModalBody>
                                <ModalBody className="team-container">
                                <Container className="team-container2">
                                    <Row >
                                    <Col sm={{ size: 5, offset: 0 }} className="team-font-top">
                                        <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/> Simaru X</Col>
                                    <Col sm={{ size: 5, offset: 2 }} className="team-font-top">
                                        <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/> Big Box</Col>
                                    </Row>
                                    <Row>
                                    <Col sm={{ size: 5, offset: 0 }}className="team-font">
                                        <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/>  Smart Eye</Col>
                                    <Col sm={{ size: 5, offset: 2 }}className="team-font">
                                        <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/> TOMPS</Col>
                                    </Row>
                                </Container>
                                </ModalBody>
                                <ModalBody className="details-title2">
                                Time Slot
                                </ModalBody>
                                <ModalBody className="time-container">
                                <Container>
                                <Row>
                                    <Col sm={{size: 1, offset: 0}}>
                                        <img className="icon-time" src={iconSchedule2} alt='iconSchedule2'/>
                                    </Col>
                                    <Col sm={{size: 11, offset: 0}}>
                                        <span className="time-date">September 14,2021</span>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={{size: 1, offset: 0}}>
                                        <img className="icon-time" src={iconMeetingTime} alt='iconMeetingTime'/>
                                    </Col>
                                    <Col className="time-col" sm={{size: 3, offset: 0}}>09.00-10.00
                                    </Col>
                                    <Col className="time-col" sm={{size: 3, offset: 1}}>11.00-13.00
                                    </Col>
                                    <Col className="time-col" sm={{size: 3, offset: 1}}>14.00-15.00
                                    </Col>
                                </Row>
                                </Container>
                                </ModalBody>
                            </ModalBody>
                        </Modal> 
                        <Row className="icon-margin">
                        <Col className="icon-container" sm={{ size: 'auto', offset: "auto" }}><img src={iconSchedule} alt='iconSchedule'/> {list.totalSchedule} Schedule</Col>
                        <Col className="icon-container" sm={{ size: 'auto', offset: "auto" }}><img src={iconTeams} alt='iconTeams'/> {list.totalTeam} Teams</Col>
                        <Col className="icon-container2" sm={{ size: 'auto'}} style={{color: '#000'}}>{list.status}</Col>
                        </Row>
                    </Col>
                    
                        
                        {/* THIS IS MODAL FOR JOIN EVENT  */}
                    <Col sm={{size: 4, offset: "auto"}}>
                        <Button style={{
                            backgroundColor:"#095571", 
                            float: "right", 
                            color:"#EBEBEB", 
                            borderColor:"#095571", 
                            width:"100%",
                            maxWidth:"100px",  
                            margin: "22px 0px 22px 10px"}} 
                            onClick={modalToggleJoinEvent}>Join{buttonLabel}</Button>
                    
                    </Col>
                        <Modal isOpen={modalJoinEvent} 
                        toggle={modalToggleJoinEvent} 
                        className={className} 
                        style={{
                            textAlign:"center", 
                            fontFamily: 'Roboto'}}>

                            <ModalHeader style={{
                                borderBlockColor:"white", 
                                height:"50px"}}
                                toggle={modalToggleJoinEvent}></ModalHeader>

                                <ModalBody style={{height: "400px"}}>
                                    <img src={confirmModal} alt='deleteEvent'/>
                                <ModalBody className="card-confirm1">Join Event</ModalBody>
                                <ModalBody className="card-note">Are your sure to join this event?</ModalBody> 
                                <ModalBody style={{
                                    textAlign: "center", 
                                    marginBottom:"20px" }}>
                                    <Row >
                                    <Col className="submit-pop">
                                        <Button style={{
                                            backgroundColor:"#095571", 
                                            borderColor:"#095571", 
                                            width:"100%",
                                            maxWidth:"160px",  
                                            height:"48px", 
                                            fontWeight: "bold"}}
                                            onClick={modalToggleJoinEvent} >Cancel</Button>
                                    </Col>
                                    <Col>
                                        <Button style={{
                                            backgroundColor:"#EA492C", 
                                            borderColor:"#EA492C", 
                                            width:"100%",
                                            maxWidth:"160px", 
                                            height:"48px",
                                            fontWeight: "bold"}} 
                                            onClick={toggleNestedJoinEvent} disabled>Join</Button>
                                    </Col>
                                    </Row>
                                </ModalBody>

                                    <Modal isOpen={nestedmodalJoinEvent} 
                                    toggle={toggleNestedJoinEvent} 
                                    onClosed={closeAllJoinEvent ? modalToggleJoinEvent : undefined} 
                                    style={{
                                        textAlign:"center", 
                                        fontFamily: 'Roboto'}}>
                                    <ModalHeader style={{
                                        borderBlockColor:"white", 
                                        height:"50px"}}
                                        toggle={modalToggleJoinEvent}></ModalHeader>
                                    <ModalBody style={{
                                        height: "400px", 
                                        paddingTop: "30px"}}>
                                            <img src={missionComplete} alt='eventDeleted'/>
                                        <ModalBody className="card-confirm2">Mission Complete</ModalBody>
                                        <ModalBody className="card-note">Your request has been created.</ModalBody>
                                        <Button style={{
                                            backgroundColor:"#095571", 
                                            borderColor:"#095571", 
                                            width:"100%",
                                            maxWidth:"160px",  
                                            height:"48px", 
                                            fontWeight: "bold"}} 
                                            onClick={toggleAllJoinEvent}>Back to Event</Button>
                                    </ModalBody>
                                    </Modal>
                                    
                                </ModalBody>
                        </Modal>
                    </Row>
                </Container>
                </CardBody>
            
            )):
            // {/* ----- Display Monev in Your Event End Code ----- */}
            <CardBody className="card-body-content">
            <img src={noEvents} alt='noEvents'/>
            <h4 className="oops">Oops!</h4>
            <h5 className="input-spacing">You don't have any events.</h5>
          </CardBody>
        }
            </CardBody>
            </Card>
        </Col>
        </Row>
    </Container>
    );
};

export default InnovatorHomePage;