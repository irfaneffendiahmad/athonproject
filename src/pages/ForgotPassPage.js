import React, { useState } from 'react';
import {
    Card, CardBody, Form, FormGroup, Label, Input, Button, Container, Row, Col
  } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import bgForgotPass1 from '../assets/bgForgotPass1.svg';
import bgLogin2 from '../assets/bgLogin2.svg';
import logo from '../assets/logo.svg';
import { NotificationManager } from 'react-notifications';


const ForgotPassPage = () => {

        // -1- Prepare to collect email data
    const [forgotPassword, setForgotPassword] = useState({
        email: '',
    });
    
    const handleForgotPassword = (e) => {
        setForgotPassword({
            ...forgotPassword,
            [e.target.name] : e.target.value
        })
    }
    // -1-

    // -2- Grab email address from API
    const history = useHistory();
    
    const handleSubmitForgotPass = (e) => {
        e.preventDefault();

        axios({
            method: 'POST',
            url: 'https://dev-team4-backend.herokuapp.com/api/v1/auth/sendEmailResetPassword',
            data: JSON.stringify({
                email: forgotPassword.email,
            }),
            headers: {
                'Content-Type': 'application/json',
            }
        }).then((response) => {
                console.log('response =>', response);
                history.push('/ForgotPassResp');
        })
        .catch(err => {
                console.log('error =>', err);
                NotificationManager.error('Your email is invalid!','',3000);
            });
        }
    // -2-

    // -3- Internal CSS Style
    const myInput = {
        width: '100%',
        maxWidth: '16em',
        backgroundColor: 'transparent',
        color: '#fff'
    }
    // -3-

    return (

        <Container fluid style={{width: '100%'}}>
        <Row>
            <Col style={{backgroundColor:"#EBEBEB"}}>
                <img src={bgForgotPass1} alt='bgForgotPass1.svg' 
                style={{
                    width: '100%', 
                    marginTop: "8%" }}/>
            </Col>
            <Col md={4} style={{
                backgroundImage: `url(${bgLogin2})`, 
                height: '100vh'}}>
            <center>
                <Card style={{ 
                    width: '100%', 
                    maxWidth: '18rem', 
                    backgroundColor: '#095571', 
                    border: 'none',
                    marginTop: '30%' }}>
                <CardBody>
                <img src={logo} alt='logo.svg' className="App-logo"/><p/>
                <Form style={{color: '#fff'}}>
                    <FormGroup style={{textAlign: 'left'}}>
                        <div style={{fontSize: 'x-large'}}>
                            Forgot Password?
                        </div>
                        <p style={{fontSize: 'medium'}}>
                            Enter email address associated with your account
                        </p>
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Email address</Label>
                        <Input 
                            type="email" 
                            placeholder="Enter your email"
                            name="email"
                            className="form-control"
                            style={myInput}
                            onChange={handleForgotPassword} />
                    </FormGroup><p />

                    <Button 
                        onClick={handleSubmitForgotPass}
                        style={{
                            backgroundColor: '#F9CC2C', 
                            color: 'black', 
                            width: '16rem'}}
                        type="submit">
                            Submit
                    </Button>

                </Form>
                </CardBody>
                </Card>
            </center>
            </Col>
        </Row>
    </Container>
        
    );

}

export default ForgotPassPage;