import React, { useState } from 'react';
import {
    Card, CardBody, Form, FormGroup, Label, Input, Button, Container, Row, Col
  } from 'reactstrap';
// import qs from 'qs';
import axios from 'axios';
import bgRegister from '../assets/bgRegister.svg';
import bgLogin2 from '../assets/bgLogin2.svg';
import logo from '../assets/logo.svg';
import { NotificationManager} from 'react-notifications';
import { useHistory } from 'react-router-dom';


const RegisterPage = () => {

        // -1- Prepare to collect registration data
        const [regData, setRegData] = useState({
            name: '',
            role: '',
            email: '',
            password: '',
            repeatPassword: '',
        });
    
        const history = useHistory();
    
        const handleChangeReg = (e) => {
            setRegData({
                ...regData,
                [e.target.name] : e.target.value
            })
        }
        // -------
    
        // -2- Grab data to register to database by API
        const handleSubmitReg = (e) => {
            e.preventDefault();
            axios({
                method: 'POST',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/auth/register',
                // url: 'https://reqres.in/api/register',
                data: JSON.stringify({
                // data: qs.stringify({
                    name: regData.name,
                    role: regData.role,
                    email: regData.email,
                    password: regData.password,
                    repeatPassword: regData.repeatPassword,
                }),
                headers: {
                    'Content-Type': 'application/json',
                    // 'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                }
            }).then((response) => {
                  console.log('response =>', response);
                  NotificationManager.info('S U C C E S S !','',3000);
                  history.push('/');
            })
              .catch(err => {
                console.log('error =>', err);
                NotificationManager.error(err,'',3000);
            });
        }
        // -2-
    
        // -3- Internal CSS Style
        const myInput = {
            width: '100%',
            maxWidth: '16em',
            backgroundColor: 'transparent',
            color: '#fff'
        }
        // -3-

    return (

        <Container fluid style={{width: '100%'}}>
        <Row>
            <Col style={{backgroundColor:"#EBEBEB"}}>
                <img src={bgRegister} alt='bgLogin1' style={{ width: '100%', marginTop: "8%"}}/>
            </Col>
            <Col md={4} style={{backgroundImage: `url(${bgLogin2})`, height: '100vh'}}>
            <center>
                <Card style={{ width: '100%', maxWidth: '18rem', backgroundColor: '#095571', border: 'none', marginTop: '18%' }}>
                <CardBody>
                <img src={logo} alt='logo.svg' className="App-logo"/><p/>
                <Form style={{color: '#fff'}}>
                    <FormGroup style={{textAlign: 'left'}}>
                        <div style={{fontSize: 'x-large'}}>
                            Join us!
                        </div><p/>
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Name</Label>
                        <Input 
                            type="text" 
                            placeholder="Enter your full name"
                            name="name"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} />
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Role</Label>
                        <Input 
                            type="select" 
                            placeholder="Pick your role"
                            name="role"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg}>
                                <option selected disabled>Choose your role</option>
                                <option>Innovator</option>
                                <option>Innovation Manager</option>
                        </Input>
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Email address</Label>
                        <Input 
                            type="email" 
                            placeholder="Enter your email"
                            name="email"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} />
                        {/* <Input 
                            type="email" 
                            placeholder="eve.holt@reqres.in"
                            name="email"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} /> */}
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>New Password</Label>
                        <Input 
                            type="password"
                            placeholder="Enter your password"
                            name="password"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} />
                        {/* <Input 
                            type="password"
                            placeholder="pistol"
                            name="password"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} /> */}
                    </FormGroup><p/>


                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Verify Password</Label>
                        <Input 
                            type="password"
                            placeholder="Enter your password"
                            name="repeatPassword"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} />
                        {/* <Input 
                            type="password"
                            placeholder="pistol"
                            name="password"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReg} /> */}
                    </FormGroup><p/>

                    <Button 
                        onClick={handleSubmitReg}
                        style={{backgroundColor: '#F9CC2C', color: 'black', width: '16rem'}}
                        type="submit">
                            Sign up
                    </Button>

                    <p style={{textAlign: 'center', fontSize: 'small'}}>
                        Have an account?
                        <a href="/" style={{color: '#F9CC2C', textDecoration: 'none'}}>
                            <b> Sign in</b>
                        </a>
                    </p>

                </Form>
                </CardBody>
                </Card>
            </center>
            </Col>
        </Row>
    </Container>
        
    );

}

export default RegisterPage;