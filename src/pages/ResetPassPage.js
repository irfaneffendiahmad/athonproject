import React, { useState } from 'react';
import {
    Card, CardBody, Form, FormGroup, Label, Input, Button, Container, Row, Col
  } from 'reactstrap';
import axios from 'axios';
import bgResetPass from '../assets/bgResetPass.svg';
import bgLogin2 from '../assets/bgLogin2.svg';
import logo from '../assets/logo.svg';
import { NotificationManager} from 'react-notifications';
import { useHistory } from 'react-router-dom';

const ResetPassPage = () => {

        // -1- Prepare to collect reset data
        const [resetData, setResetData] = useState({
            token: '',
            newPassword: '',
            repeatPassword: ''
        });

        const history = useHistory();
    
        const handleChangeReset = (e) => {
            setResetData({
                ...resetData,
                [e.target.name] : e.target.value
            })
        }
        // -------
    
        // -2- Grab data to reset from API
        const url_string = window.location.href;
        const url = new URL(url_string);
        const getToken = url.searchParams.get('token');

        const handleSubmitReset = (e) => {
            e.preventDefault();
            axios({
                method: 'POST',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/auth/resetPassword',
                data: JSON.stringify({
                    token: getToken,
                    newPassword: resetData.newPassword,
                    repeatPassword: resetData.repeatPassword,
                }),
                headers: {
                    'Content-Type': 'application/json',
                }
            }).then((response) => {
                    console.log('response =>', response);
                    NotificationManager.info('S U C C E S S !','',3000);
                    history.push('/');
            })
            .catch(err => {
                    console.log('error =>', err);
                    NotificationManager.error(err,'',3000);
            });
        }
        // -2-

    
        // -3- Internal CSS Style
        const myInput = {
            width: '100%',
            maxWidth: '16em',
            backgroundColor: 'transparent',
            color: '#fff'
        }
        // -3-

    return (

        <Container fluid style={{width: '100%'}}>
        <Row>
            <Col style={{backgroundColor:"#EBEBEB"}}>
                <img src={bgResetPass} alt='bgLogin1' style={{ width: '100%', marginTop: "8%" }}/>
            </Col>
            <Col md={4} style={{backgroundImage: `url(${bgLogin2})`, height: '100vh'}}>
            <center>
                <Card style={{ width: '100%', maxWidth: '18rem', backgroundColor: '#095571', border: 'none', marginTop: '25%' }}>
                <CardBody>
                <img src={logo} alt='logo.svg' className="App-logo"/><p/>
                <Form style={{color: '#fff'}}>
                    <FormGroup style={{textAlign: 'left'}}>
                        <div style={{fontSize: 'x-large'}}>
                            Forgot Password?
                        </div><p/>
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>New Password</Label>
                        <Input 
                            type="password"
                            placeholder="Enter your new password"
                            name="newPassword"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReset} />
                    </FormGroup>

                    <FormGroup style={{textAlign: 'left'}}>
                        <Label>Verify New Password</Label>
                        <Input 
                            type="password"
                            placeholder="Re-enter your new password"
                            name="repeatPassword"
                            className="form-control"
                            style={myInput}
                            onChange={handleChangeReset} />
                    </FormGroup><p/>

                    <Button 
                        onClick={handleSubmitReset}
                        style={{backgroundColor: '#F9CC2C', color: 'black', width: '16rem'}}
                        type="submit">
                            Reset
                    </Button>
                    
                    <p style={{textAlign: 'center', fontSize: 'small'}}>
                        Have reset your password?
                        <a href="/" style={{color: '#F9CC2C', textDecoration: 'none'}}>
                            <b> Sign in</b>
                        </a>
                    </p>

                </Form>
                </CardBody>
                </Card>
            </center>
            </Col>
        </Row>
    </Container>
        
    );

}

export default ResetPassPage;