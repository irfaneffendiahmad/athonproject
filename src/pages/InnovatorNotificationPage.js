import React, {useState, useEffect} from 'react';
import { Badge, Card, CardTitle, Container, Row, Col, Button, CardBody,  } from 'reactstrap';
import "../assets/css/notificationpage.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faTimes, faClock} from "@fortawesome/free-solid-svg-icons";
import axios from 'axios';


const InnovatorNotificationPage = (props) => {

    // -1- Get Request Data
    const [requestData, setRequestData] = useState([]);
    const [requestCount, setRequestCount] = useState(6);

    useEffect(() => {
        const fetchRequestData = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/notification/innovator/request',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for request notification data =>', response.data.data);
                setRequestData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchRequestData();
    }, []);
    // -1-


    // -2- Get Activity Data
    const [activityData, setActivityData] = useState([]);
    const [activityCount, setActivityCount] = useState(8);

    useEffect(() => {
        const fetchActivityData = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/notification/activity',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for activity notification data =>', response.data.data);
                setActivityData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchActivityData();
    }, []);
    // -2-


    //MODAL APPROVE//
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);


    //MODAL REJECT//
    const [modal1, setModal1] = useState(false);
    const toggle1 = () => setModal1(!modal1);


    // -3- Load More Function for Other Notification
    const loadMoreRequest = () => {
        setRequestCount(requestData.length);
    }
    // -3-

    
    // -4- Load More Function for Other Notification
    const loadMoreActivity = () => {
        setActivityCount(activityData.length);
    }
    // -4-


    // -5- Delete Other Notifications
    const delOtherNotification = (id) => {
        const url = `https://dev-team4-backend.herokuapp.com/api/v1/notification/activity/${id}`;

        axios({
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
                console.log('response for your other notification deletion =>', response.data.data);
                window.location.reload();
        })
        .catch(err => {
                console.log('error =>', err);
        });
    }
    // -5-


    return (
       <Container className="container-whole">
           <Row className="template">

            {/* //INVITATIONS AND APPROVAL// */}

            <Col sm={{ size: 6, offset: 0.5}}>
                <Card>
                    <CardTitle style={{
                        height: '', 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"}}>
                       <p className="JudulNotif">Invitations and Approval</p>
                    </CardTitle >


                    {/* //CARD BODY 1 (INVITATION)// */}

                    <CardBody  style={{
                        height: '100%', 
                        marginTop:'-20px', 
                        marginBottom:'0px',
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"
                        }}>
                        
                    {/* ----- Display Requests Notification Begin Code ----- */}
                    {requestData.slice(0, requestCount).map((list, i) => (

                        <Row>
                            <Col sm={{ size: 10, offset: 0}}>
                                <Row >
                                    <Col sm={{size: 2, offset: 0}} style={{
                                        verticalAlign:"middle", 
                                        marginRight:'14px'}}>
                                        <Badge style={{
                                            backgroundColor:'#F9CC2C', 
                                            fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif", color: "black"
                                            }}>
                                                {list.type}
                                        </Badge>
                                    </Col >


                                    <Col sm={{size: 9, offset: 0 }} style={{
                                        marginTop:"4px",color:"#979797", 
                                        fontSize:"10px", 
                                        fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                        }}>
                                        <FontAwesomeIcon icon={faClock} style={{color:'#979797'}}/> &nbsp;
                                        {Date(list.createdAt)}
                                    </Col>
                                </Row>

                                <Row style={{marginBottom:'-13px'}}>
                                <p style={{
                                    color:"black", 
                                    fontSize:"12px", 
                                    fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                    }}>
                                        {list.message}
                                </p> 

                                </Row>
                               
                            </Col >


                            <Col sm={{ size: 2, offset: 0.3}} style={{
                                alignItems:'center', 
                                marginTop:'20px', 
                                float:'right'
                                }}>
                               
                                <Button disabled
                                 style={{
                                    backgroundColor:'#095571', 
                                    borderColor:'##095571', 
                                    color:'#FAFAFA', 
                                    fontSize:'12px',
                                    fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                }}>Request</Button>{' '}
                                  
                            </Col>
                        </Row>

                    ))}
                    {/* ----- Display Requests Notification End Code ----- */}

                    </CardBody>

                    {/* //LOAD MORE RESULTS// */}

                    <CardBody style={{
                        textAlign:'center', 
                        marginTop:'15px', 
                        marginBottom:'15px' }}> 
                        <Button style={{
                            backgroundColor:'#EBEBEB', 
                            borderColor:'#EBEBEB', 
                            color:'#231F20', 
                            fontSize:'12px',
                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                            }} onClick={loadMoreRequest}>Load More Results
                        </Button>{' '}
                    </CardBody>

                </Card>
            </Col>

{/* /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// */}
           <Col sm={{ size: 6, offset: 0.5}} style={{
               borderBottom: 'solid',
               borderWidth:'1px', 
               borderBlockColor:"#CCCCCC"}} >
                <Card >
                    <CardTitle style={{ 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"}}>
                       <p className="JudulNotif">Other Notifications</p>
                    </CardTitle >


                    {/* //CARD BODY 1 (REMINDER)// */}

                    <CardBody  style={{
                        height: '100%', 
                        marginTop:'-20px', 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"}}>
                    
                    {/* ----- Display Activity Notification Begin Code ----- */}
                    {activityData.slice(0, activityCount).map((list, i) => (

                        <Row>
                            <Col sm={{ size: 1, offset: 0.5}} xs={{ size: 2, offset:0 }}>
                                <Button style={{
                                    backgroundColor:"#C4C4C4", 
                                    borderColor:'#C4C4C4',
                                     width:'25px', 
                                     height: '30px', 
                                     backgroundPosition:'center',
                                     }} onClick={() => delOtherNotification(list.id)} disabled>
                                         <FontAwesomeIcon icon={faTimes} style={{
                                             color:'white',
                                             marginLeft:'-5px', 
                                             marginBottom:'10px'
                                             }}/>
                                </Button>
                            </Col>

                            <Col sm={{ size: 11, offset: 0.3}} xs={{ size: 10, offset:0 }}>
                                <Row style={{marginLeft:'9px'}} >
                                    <Col sm={{size: 1, offset: 0}} xs={{ size: 5, offset:0 }} style={{verticalAlign:"middle"}}>
                                        <Badge style={{
                                            backgroundColor:'#009927', 
                                            fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }}>{list.type}
                                        </Badge>
                                    </Col >

                                    <Col sm={{size: 11, offset: 0}} xs={{ size: 11, offset: -3}} style={{
                                        float:'right', 
                                        marginTop:"4px",
                                        marginLeft:'-25px',
                                        color:"#979797", 
                                        fontSize:"10px", 
                                        fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                        }}>
                                        <p style={{float:'right'}}>
                                            <FontAwesomeIcon icon={faClock} style={{color:'#979797'}}/>
                                             &nbsp;{Date(list.createdAt)}
                                        </p>
                                        
                                        
                                    </Col>
                                </Row>

                                <Row style={{
                                    marginBottom:'-13px', 
                                    marginTop:'-10px', 
                                    marginLeft:'9px'}}>
                                <p style={{
                                    color:"black", 
                                    fontSize:"12px", 
                                    fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                    }}>
                                    {list.message}
                                </p> 

                                </Row>

     
                            </Col>
                        </Row>

                    ))}
                    {/* ----- Display Activity Notification End Code ----- */}

                    </CardBody>

                    {/* //LOAD MORE RESULTS// */}

                    <CardBody style={{
                        textAlign:'center', 
                        marginTop:'15px', 
                        marginBottom:'15px' }}> 
                        <Button onClick={loadMoreActivity} style={{
                            backgroundColor:'#EBEBEB', 
                            borderColor:'#EBEBEB', 
                            color:'#231F20', 
                            fontSize:'12px',
                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                            }}>
                                Load More Results
                        </Button>{' '}
                    </CardBody>
                    


                </Card>
            </Col>     
           
            
           </Row>

       </Container>
    );

}

export default InnovatorNotificationPage;