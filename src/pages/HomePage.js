import React, {useState, useEffect} from 'react';
import { 
  TabContent,
  Container, 
  CardBody, 
  TabPane, 
  Nav, 
  NavItem, 
  NavLink, 
  Card, 
  Button, 
  FormGroup, 
  Form, 
  Label, 
  Input, 
  CardText, 
  Row, 
  Col, 
  Table, 
  Modal, 
  ModalHeader, 
  ModalBody, 
  ModalFooter} from 'reactstrap';
import classnames from 'classnames';
import Select from 'react-select';
import "../assets/css/homepage.css";
import confirmModal from "../assets/confirmModal.svg";
import missionComplete from "../assets/missionComplete.svg";
// import deleteComplete from "../assets/deleteComplete.svg";
import noEvents from "../assets/noEvents.svg";
import iconSchedule from "../assets/iconSchedule.svg";
import iconTeams from "../assets/iconTeams.svg";
import iconEdit from "../assets/iconEdit.svg";
import iconDelete from "../assets/iconDelete.svg";
import iconTeamInvited from "../assets/iconTeamInvited.svg";
import iconSchedule2 from "../assets/iconSchedule2.svg";
import iconMeetingTime from "../assets/iconMeetingTime.svg";
import axios from 'axios';


const HomePage = (props) => {

// -1- Grab Team data
const [teamData, setTeamData] = useState([]);

const optTeamData = teamData?.reduce((acc, data) => {
  acc.push({
    label: data.name,
    value: data.id,
  });
  return acc;
}, []);

useEffect(() => {
  const fetchData = async () => {
    axios({
        method: 'GET',
          url: 'https://dev-team4-backend.herokuapp.com/api/v1/team/find-team',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
          }
    }).then((response) => {
        console.log('response for team data =>', response.data);
        setTeamData(response.data.data);
    }).catch(err => {
        console.log('error =>', err);
    });
  }
  fetchData();
}, []);
// -1-


// -2- General Modal Function
const [isOpenModalEvent, setIsOpenModalEvent] = useState(false);
// const [isOpenSuccModalEvent, setIsOpenSuccModalEvent] = useState(false);
// const [closeAllEvent, setcloseAllEvent] = useState(false);


// -3- Prepare to collect Event data
const [createEventForm, setCreateEventForm] = useState({
    createEventField: '',
    participants: [
        {teamId1: ''},
        {teamId2: ''},
        {teamId3: ''}
    ]
});

const setFieldEvent = (field, value) => {
  setCreateEventForm({
    ...createEventForm,
    [field]: value,
  });
}

const handleCreateEvent = () => {
  setIsOpenModalEvent(true);
}
// -3-


// -4- Submit Create Event data to database by API
const submitCreateEvent = (e) => {
  e.preventDefault();
  setIsOpenModalEvent(false);

  if (submitCreateEvent) {
    if ((!createEventForm.teamId3 && !createEventForm.teamId2 && !createEventForm.teamId1) || (!createEventForm.teamId3 && !createEventForm.teamId1) || (!createEventForm.teamId2 && !createEventForm.teamId1)) {
      modalToggleEvent();
    }
    else if (!createEventForm.teamId3 && !createEventForm.teamId2) {
      axios({
          method: 'POST',
          url: 'https://dev-team4-backend.herokuapp.com/api/v1/event',
          data: JSON.stringify({
              name: createEventForm.createEventField,
              participants: [
                  {teamId: parseInt(createEventForm.teamId1.value)}
              ]
          }),
          headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
          }
      }).then((response) => {
            console.log('response create event submission =>', response);
            window.location.reload();
      }).catch(err => {
          console.log('error =>', err);
          modalToggleEvent();
      });
    }
    else if (!createEventForm.teamId3) {
      axios({
          method: 'POST',
          url: 'https://dev-team4-backend.herokuapp.com/api/v1/event',
          data: JSON.stringify({
              name: createEventForm.createEventField,
              participants: [
                  {teamId: parseInt(createEventForm.teamId1.value)},
                  {teamId: parseInt(createEventForm.teamId2.value)}
              ]
          }),
          headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
          }
      }).then((response) => {
            console.log('response create event submission =>', response);
            window.location.reload();
      }).catch(err => {
          console.log('error =>', err);
          modalToggleEvent();
      });
    }
    else {
      axios({
          method: 'POST',
          url: 'https://dev-team4-backend.herokuapp.com/api/v1/event',
          data: JSON.stringify({
              name: createEventForm.createEventField,
              participants: [
                  {teamId: parseInt(createEventForm.teamId1.value)},
                  {teamId: parseInt(createEventForm.teamId2.value)},
                  {teamId: parseInt(createEventForm.teamId3.value)},
              ]
          }),
          headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
          }
      }).then((response) => {
            console.log('response create event submission =>', response);
            window.location.reload();
      }).catch(err => {
          console.log('error =>', err);
          modalToggleEvent();
      });
    }
  }
}

console.log('createEventForm =>', createEventForm);
// -4-


// -5- Display event in Your Event column
const [displayEvent, setDisplayEvent] = useState([]);

useEffect(() => {
    const fetchDataYourEvent = async () => {
        axios({
            method: 'GET',
            url: 'https://dev-team4-backend.herokuapp.com/api/v1/event',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
            console.log('response for your event column =>', response.data.data);
            setDisplayEvent(response.data.data);
        })
        .catch(err => {
            console.log('error =>', err);
        });
    }
    fetchDataYourEvent();
}, []);
// -5-


// -6- Grab Event data
const [eventData, setEventData] = useState([]);

const optEventData = eventData?.reduce((acc, data) => {
  acc.push({
    label: data.event,
    value: data.id,
  });
  return acc;
}, []);

useEffect(() => {
  const fetchEventData = async () => {
    axios({
        method: 'GET',
          url: 'https://dev-team4-backend.herokuapp.com/api/v1/event',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
          }
    }).then((response) => {
        console.log('response for event data =>', response.data);
        setEventData(response.data.data);
    }).catch(err => {
        console.log('error =>', err);
    });
  }
  fetchEventData();
}, []);
// -6-


// -7- Delete Team in Your Team Display
const handleDeleteEvent = (id) => {
  const url = `https://dev-team4-backend.herokuapp.com/api/v1/event/${id}`;

  axios({
      method: 'DELETE',
      url: url,
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
      }
  }).then((response) => {
          console.log('response for your team deletion =>', response.data.data);
          window.location.reload();
  })
  .catch(err => {
          console.log('error =>', err);
  });
}
// -7-


// -8- Submit Schedule Event data to database by API
const submitScheduleEvent = (e) => {
  e.preventDefault();
  setIsOpenModalEvent(false);

  axios({
      method: 'POST',
      url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule',
      data: JSON.stringify({
        eventId: parseInt(createEventForm.scheduleEventField.value),
        date: createEventForm.scheduleDate,
        times: [{
            start: createEventForm.startTime1,
            end: createEventForm.endTime1
        },{
            start: createEventForm.startTime2,
            end: createEventForm.endTime2
        },{
            start: createEventForm.startTime3,
            end: createEventForm.endTime3
        },{
            start: createEventForm.startTime4,
            end: createEventForm.endTime4
        }]
      }),
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
      }
  }).then((response) => {
        console.log('response create schedule submission =>', response);
        setmodalSchedule(!modalSchedule);
  }).catch(err => {
      console.log('error =>', err);
  });
}

// -8-


// -9- Grab Schedule data for Your Event column
const [displayEventSchedule, setDisplayEventSchedule] = useState([]);

useEffect(() => {
    const fetchDataYourEventSchedule = async () => {
        axios({
            method: 'GET',
            url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
            console.log('response for schedule in your event column =>', response.data.data);
            setDisplayEventSchedule(response.data.data);
        })
        .catch(err => {
            console.log('error =>', err);
        });
    }
    fetchDataYourEventSchedule();
}, []);
// -9-


// PAGE STYLE (MODAL FUNCTION FOR CREATING EVENT)
const {
  buttonLabel,
  className
} = props;

const modalToggleEvent = () => setIsOpenModalEvent(false);

// const toggleNestedEvent = () => {
//     setIsOpenSuccModalEvent(false);
//     setcloseAllEvent(false);
// }


// MODAL FUNCTION FOR CREATING SCHEDULE//
const [modalSchedule, setmodalSchedule] = useState(false);
const [nestedmodalSchedule, setNestedmodalSchedule] = useState(false);
const [closeAllSchedule, setcloseAllSchedule] = useState(false);

const modalToggleSchedule = () => setmodalSchedule(!modalSchedule);
const toggleNestedSchedule = () => {
  setNestedmodalSchedule(!nestedmodalSchedule);
  setcloseAllSchedule(false);
}
const toggleAllSchedule = () => {
  setNestedmodalSchedule(!nestedmodalSchedule);
  setcloseAllSchedule(true);
}


// MODAL FUNCTION FOR DELETING EVENT//
const [indexDelEvent, setIndexDelEvent] = useState({});

const [modalDelEvent, setmodalDelEvent] = useState(false);
const [nestedmodalDelEvent, setNestedmodalDelEvent] = useState(false);

const modalToggleDelEvent = (indexDelEvent) => {
  setmodalDelEvent(!modalDelEvent);
  setIndexDelEvent(indexDelEvent);
}
const toggleNestedDelEvent= () => {
  handleDeleteEvent(indexDelEvent);
  setNestedmodalDelEvent(!nestedmodalDelEvent);
}


// MODAL FUNCTION FOR EVENT DETAILS VIEW ONLY//
const [indexToggleEventDetailsListId, setIndexToggleEventDetailsListId] = useState();
const [indexToggleEventDetailsListEvent, setIndexToggleEventDetailsListEvent] = useState();
const [indexToggleEventDetailsParticipants, setIndexToggleEventDetailsParticipants] = useState([]);
const [arrayDisplayEventScheduleForViewOnly, setArrayDisplayEventScheduleForViewOnly] = useState([]);

const [modalEventDetails, setmodalEventDetails] = useState(false);
const toggleEventDetails = (indexToggleEventDetailsListId, indexToggleEventDetailsListEvent, [indexToggleEventDetailsParticipants]) => {
  setmodalEventDetails(!modalEventDetails);
  setIndexToggleEventDetailsListId(indexToggleEventDetailsListId);
  setIndexToggleEventDetailsListEvent(indexToggleEventDetailsListEvent);
  setIndexToggleEventDetailsParticipants(indexToggleEventDetailsParticipants);

  axios({
    method: 'GET',
    url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
    }
  }).then((response) => {
      setArrayDisplayEventScheduleForViewOnly(response.data.data);
  })
  .catch(err => {
      console.log('error =>', err);
  });
}
const toggleEventDetailsX = () => setmodalEventDetails(!modalEventDetails);


// MODAL FUNCTION FOR EDIT EVENTS//
const [indexEditEventListId, setIndexEditEventListId] = useState();
const [indexEditEventListEvent, setIndexEditEventListEvent] = useState();
const [indexEditEventListParticipants, setIndexEditEventListParticipants] = useState([]);
const [arrayDisplayEventScheduleForEditEvent, setArrayDisplayEventScheduleForEditEvent] = useState([]);

const [modalEditEvents, setmodalEditEvents] = useState(false);
const [nestedmodalEditEvents, setNestedmodalEditEvents] = useState(false);
const [closeAllEditEvents, setcloseAllEditEvents] = useState(false);

const modalToggleEditEvents = (indexEditEventListId, indexEditEventListEvent, [indexEditEventListParticipants], arrayDisplayEventScheduleForEditEvent) => {
  setmodalEditEvents(!modalEditEvents);
  setIndexEditEventListId(indexEditEventListId)
  setIndexEditEventListEvent(indexEditEventListEvent)
  setIndexEditEventListParticipants(indexEditEventListParticipants);

  axios({
    method: 'GET',
    url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
    }
  }).then((response) => {
      setArrayDisplayEventScheduleForEditEvent(response.data.data);
  })
  .catch(err => {
      console.log('error =>', err);
  });

  // console.log('LIST ID value =>', indexEditEventListId);
  // console.log('ARRAY SCHEDULE data =>', arrayDisplayEventScheduleForEditEvent);
}
const modalToggleEditEventsDiscard = () => setmodalEditEvents(!modalEditEvents);


const toggleNestedEditEvents= () => {
  setNestedmodalEditEvents(!nestedmodalEditEvents);
  setcloseAllEditEvents(false);
}
const toggleAllEditEvents = () => {
  setNestedmodalEditEvents(!nestedmodalEditEvents);
  setcloseAllEditEvents(true);
}


// TAB NAVIGATION FUNCTION//
const [activeTab, setActiveTab] = useState('1');
const toggleTab = tab => {
if(activeTab !== tab) setActiveTab(tab);
}


    return (

      <Container fluid className="container-whole"> 
        {/* THIS IS CARD FOR CREATING EVENT */}
        <Row className="template">
          <Col sm={{ size: 6, offset: 0.5}} className="col-spacing">
            <Card style={{height: '100%'}}>
              <Nav tabs>
                <NavItem >
                  <NavLink 
                  style= {{color: "#095571"}}
                  className={classnames({ active: activeTab === '1' })}
                  onClick={() => { toggleTab('1'); }}>
                  Event
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                  style= {{color: "#095571" }}
                  className={classnames({ active: activeTab === '2' })}
                  onClick={() => { toggleTab('2'); }}>
                  Schedule
                  </NavLink>
                </NavItem>
              </Nav>

            {/* EVENT TAB GOES HERE */}
            <TabContent activeTab={activeTab}>
              <TabPane tabId="1">
              <Row>
                <Col>
                  
                <Card style={{height: "585px", border: "1px solid white"}}>
                  <h3 className="card-headings">Create Event</h3>
                    <CardBody className="card-body-wrapper1">
                      <CardText>
                        <Form>
                          <FormGroup>
                            <Label className="form-spacing" for="Event">Event</Label>
                            <Input className="input-spacing"
                              type="text"
                              name="createEventField"
                              id="createEventField"
                              placeholder="Event name (min 10 chars)"
                              onChange={(e)=> setFieldEvent('createEventField', e.target.value)}
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label className="form-spacing" for="team">Team 1</Label>
                            <Select
                                className="form-spacing"
                                options={optTeamData}
                                  onChange={(selected) => setFieldEvent('teamId1', selected)}
                                  value={createEventForm.teamId1}
                                  placeholder= "Select your team"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label className="form-spacing" for="team">Team 2</Label>
                            <Select
                                className="form-spacing"
                                options={optTeamData}
                                  onChange={(selected) => setFieldEvent('teamId2', selected)}
                                  value={createEventForm.teamId2}
                                  placeholder= "Select your team"
                            />
                          </FormGroup>
                          <FormGroup>
                            <Label className="form-spacing" for="team">Team 3</Label>
                            <Select
                                className="form-spacing"
                                options={optTeamData}
                                  onChange={(selected) => setFieldEvent('teamId3', selected)}
                                  value={createEventForm.teamId3}
                                  placeholder= "Select your team"
                            />
                          </FormGroup>
                        </Form>
                      </CardText>

                        <Button style={{
                          backgroundColor:"#095571", 
                          borderColor: "#EBEBEB",
                          color:"#fff", 
                          float:"right",
                          marginTop:'25px', 
                          width:"100%",
                          maxWidth:"160px",   
                          marginBottom:'38px'}} 
                          disabled={createEventForm.createEventField.length >= 10 ? false : true}
                          onClick={ handleCreateEvent}>Create{buttonLabel}</Button>

                        <Modal isOpen={isOpenModalEvent} 
                        toggle={modalToggleEvent} 
                        className={className} 
                        style={{
                          textAlign:"center", 
                          fontFamily: "Roboto"}}>
                          
                          <ModalHeader style={{
                            borderBlockColor:"white"}}
                            toggle={modalToggleEvent}></ModalHeader>
                            <ModalBody style={{height: "400px"}}>
                              <img src={confirmModal} alt='createSchedule'/>
                              <ModalBody className="card-confirm1">Create Event</ModalBody>
                              <ModalBody className="card-note">Are you sure to create this event?</ModalBody> 
                              <ModalBody style={{
                                textAlign: "center", 
                                marginBottom:"20px" }}>
                                <Row >
                                  <Col className="submit-pop">
                                    <Button style={{
                                      backgroundColor:"#095571", 
                                      borderColor:"#095571", 
                                      width:"100%",
                                      maxWidth:"160px",  
                                      height:"48px", 
                                      fontWeight: "bold"}}
                                      onClick={modalToggleEvent} >Cancel</Button>
                                  </Col>
                                  <Col>
                                    <Button style={{
                                      backgroundColor:"#EA492C", 
                                      borderColor:"#EA492C", 
                                      width:"100%",
                                      maxWidth:"160px",  
                                      height:"48px",
                                      fontWeight: "bold"}} 
                                      onClick={ submitCreateEvent}>Create</Button>
                                  </Col>
                                </Row>
                              </ModalBody>
                            </ModalBody>
                        </Modal>
                    
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              </TabPane>

              {/* ================================== */}
              {/* SCHEDULE TAB GOES HERE */}
              {/* ================================== */}

              <TabPane tabId="2">
              <Row>
                <Col >
                <Card style={{border: "1px solid white"}}>
                  <h3 className="card-headings">Create Schedule</h3>
                    <CardBody style={{height: "540px"}}>
                      <CardText>
                        <Form>
                          <Table borderless responsive>
                            <tbody>
                              <tr>
                                <td>
                                  <FormGroup>
                                  <Label className="form-spacing" for="scheduleEventField">Event</Label>
                                      <Select
                                          className="form-spacing"
                                          options={optEventData}
                                          name="scheduleEventField"
                                          id="scheduleEventField"
                                          onChange={(selected) => setFieldEvent('scheduleEventField', selected)}
                                          value={createEventForm.scheduleEventField}
                                          placeholder= "Select your event"
                                      />

                                  </FormGroup>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <FormGroup>
                                    <Label for="scheduleDate">Date</Label>
                                    <Input 
                                      style={{color: "#979797"}}
                                      type="date"
                                      name="scheduleDate"
                                      id="scheduleDate"
                                      placeholder="date placeholder"
                                      onChange={(e)=> setFieldEvent('scheduleDate', e.target.value)}
                                    />
                                  </FormGroup>
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                       
                          <Table responsive borderless>
                            <tbody>
                              <tr>
                                <td>
                                  <p className="start">Start</p>
                                  <p className="end"> End</p>
                                </td>
                                <td>
                                  <FormGroup>
                                    <Label for="eventTime1">Time 1</Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                      name="startTime1"
                                      id="time1"
                                      placeholder="time placeholder"
                                      onChange={(e)=> setFieldEvent('startTime1', e.target.value)}
                                    /> 

                                  </FormGroup>
                                  <FormGroup>
                                    <Label for="eventTime1"></Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                      name="endTime1"
                                      id="endTime1"
                                      placeholder="time placeholder"
                                      onChange={(e)=> setFieldEvent('endTime1', e.target.value)}
                                    />

                                  </FormGroup>
                                </td>
                                <td>
                                  <FormGroup>
                                    <Label for="eventTime2">Time 2</Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                        name="startTime2"
                                        id="startTime2"
                                        placeholder="time placeholder"
                                        onChange={(e)=> setFieldEvent('startTime2', e.target.value)}
                                      />

                                  </FormGroup>
                                  <FormGroup>
                                    <Label for="eventTime2"></Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                        name="endTime2"
                                        id="endTime2"
                                        placeholder="time placeholder"
                                        onChange={(e)=> setFieldEvent('endTime2', e.target.value)}
                                      />
                                  </FormGroup>
                                </td>
                                <td>
                                  <FormGroup>
                                    <Label for="eventTime3">Time 3</Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                        name="startTime3"
                                        id="startTime3"
                                        placeholder="time placeholder"
                                        onChange={(e)=> setFieldEvent('startTime3', e.target.value)}
                                      />
                                  </FormGroup>
                                  <FormGroup>
                                    <Label for="eventTime3"></Label>
                                      <Input
                                        style={{color: "#979797"}}
                                        type="time"
                                          name="endTime3"
                                          id="endTime3"
                                          placeholder="time placeholder"
                                          onChange={(e)=> setFieldEvent('endTime3', e.target.value)}
                                        />
                                  </FormGroup>
                                </td>
                                <td>
                                  <FormGroup>
                                    <Label for="eventTime4">Time 4</Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                      name="startTime4"
                                      id="startTime4"
                                      placeholder="time placeholder"
                                      onChange={(e)=> setFieldEvent('startTime4', e.target.value)}
                                    />
                                  </FormGroup>
                                  <FormGroup>
                                    <Label for="eventTime4"></Label>
                                    <Input
                                      style={{color: "#979797"}}
                                      type="time"
                                        name="endTime4"
                                        id="endTime"
                                        placeholder="time placeholder"
                                        onChange={(e)=> setFieldEvent('endTime4', e.target.value)}
                                      />
                                  </FormGroup>
                                </td>
                              </tr>
                            </tbody>
                          </Table>
                        </Form>
                      </CardText>
                      <Button  style={{
                        backgroundColor:"#095571", 
                        borderColor:"#095571",  
                        float:"right", 
                        width:"100%",
                        maxWidth:"160px"}} 
                        onClick={modalToggleSchedule}>{buttonLabel} Create</Button>
                        <Modal isOpen={modalSchedule} 
                        toggle={modalToggleSchedule} 
                        className={className} 
                        style={{textAlign:"center", 
                        fontFamily: "Roboto"}}>
                          <ModalHeader style={{
                            borderBlockColor:"white", 
                            height:"50px"}}
                            toggle={modalToggleSchedule}></ModalHeader>
                            <ModalBody style={{height: "400px"}}>
                              <img src={confirmModal} alt='createSchedule'/>
                              <ModalBody className="card-confirm1">Submit Schedule</ModalBody>
                              <ModalBody className="card-note">Are you sure to create this schedule?</ModalBody> 
                              <ModalBody style={{
                                textAlign: "center", 
                                marginBottom:"20px" }}>
                                <Row >
                                <Col className="submit-pop">
                                      <Button style={{
                                        backgroundColor:"#095571", 
                                        borderColor:"#095571", 
                                        width:"100%",
                                        maxWidth:"160px", 
                                        height:"48px", 
                                        fontWeight: "bold"}}
                                        onClick={modalToggleSchedule} >Cancel</Button>
                                    </Col>
                                    <Col>
                                      <Button style={{
                                        backgroundColor:"#EA492C", 
                                        borderColor:"#EA492C", 
                                        width:"100%",
                                        maxWidth:"160px",  
                                        height:"48px",
                                        fontWeight: "bold"}} 
                                        onClick={submitScheduleEvent}>Create</Button>
                                    </Col>
                                </Row>
                              </ModalBody>

                                <Modal isOpen={nestedmodalSchedule} 
                                toggle={toggleNestedSchedule} 
                                onClosed={closeAllSchedule ? modalToggleSchedule : undefined} 
                                style={{
                                  textAlign:"center", 
                                  fontFamily: "Roboto"}}>
                                  <ModalHeader style={{
                                    borderBlockColor:"white", 
                                    height:"50px"}}
                                    toggle={modalToggleSchedule}></ModalHeader>
                                  <ModalBody style={{
                                    textAlign: "center", 
                                    height: "400px", 
                                    paddingTop:"20px"}}>
                                      <img src={missionComplete} alt='createSchedule'/>
                                  <ModalBody className="card-confirm2">Congratulations!</ModalBody>
                                  <ModalBody className="card-note">Your schedule has been created.</ModalBody>
                                  <Button style={{
                                    backgroundColor:"#095571", 
                                    borderColor:"#095571", 
                                    width:"100%",
                                    maxWidth:"160px",  
                                    height:"48px", 
                                    fontWeight: "bold"}} 
                                    onClick={toggleAllSchedule}>Back to Home</Button>
                                  </ModalBody>
                                </Modal>
                            </ModalBody>
                        </Modal>
                    </CardBody>
                  </Card>
                </Col>
              </Row>
              </TabPane>
            </TabContent>
            </Card>
          </Col>

          {/* THIS CARD TO SHOW YOUR EVENT */}
          {/* CARD WITH NO EVENTS NOTIFICATIONS*/}

         

          {/* ========================== */}
          {/* Display Your Event */}
          {/* ========================== */}

          
          <Col sm={{ size: 6, offset: 0}}>
            <Card>
              <CardBody className="card-body-wrapper2">
              <h3 className="card-headings" >Your Event</h3>

            
            {/* ----- Display Your Event Begin Code ----- */}
            {/* IF THERE IS AN EVENT CREATED, CARD EVENT SHOWS; OTHERWISE, NO EVENT CARD APPEARS */}
            
              {displayEvent.length > 0 ? 
              displayEvent.map((list, i) => (
                <CardBody>
                  <Container className="container-event" >
                    <Row>
                      <Col sm={{ size: 7, offset: 0 }}>
                        <Row className="monev-font" 
                        onClick={() => toggleEventDetails(list.id, list.event, [list.participants])}> {list.event}</Row>

                          <Modal isOpen={modalEventDetails} 
                          className={className} 
                          style={{fontFamily: "Roboto"}}>
                            <ModalHeader toggle={toggleEventDetailsX} style={{height: "60px"}}>
                              <h3 className="details-title1">{indexToggleEventDetailsListEvent}</h3>
                            </ModalHeader>
                              <ModalBody>
                                <ModalBody className="details-title2">
                                Team Invited
                                </ModalBody>
                                <ModalBody className="team-container">
                                  <Container className="team-container2">
                                    <Row >

                                    {indexToggleEventDetailsParticipants.map((participant,s) => (
                                        <Col key={s} sm={{ size: 5, offset: 0 }}>
                                          <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/> 
                                          <span style={{color:"black"}}>{participant.Name}</span>
                                        </Col>
                                    ))}

                                    </Row>
                                  </Container>
                                </ModalBody>
                                <ModalBody className="details-title2">
                                  Time Slot
                                </ModalBody>
                                <ModalBody className="time-container">
                                <Container>

                                  {
                                    arrayDisplayEventScheduleForViewOnly
                                    .filter(events => events.eventId === parseInt(indexToggleEventDetailsListId))
                                    .map(events => 
                                      <Row key={events.id}>
                                      <Col xs="auto">
                                        <img className="icon-time" src={iconSchedule2} alt='iconSchedule2'/>
                                        </Col>
                                        <Col xs="auto">
                                          <span className="time-date"> {events.date}</span></Col>

                                      <Col xs="auto">
                                        <img className="icon-time" src={iconMeetingTime} alt='iconMeetingTime'/>
                                        </Col>
                                        <Col xs="auto">
                                          <span className="time-date">{events.startTime} - {events.endTime}</span>
                                          </Col>
                                      </Row>)
                                  }

                                </Container>
                                </ModalBody>
                              </ModalBody>
                          </Modal> 

                        <Row className="icon-margin">
                          <Col className="icon-container" sm={{ size: 'auto', offset: 0 }}>
                            <img src={iconSchedule} alt='iconSchedule'/> Schedule</Col>
                          <Col className="icon-container">
                            <img src={iconTeams} alt='iconTeams'/> {list.participants.length} Teams</Col>
                        </Row>
                      </Col>
                      
                        
                    {/* THIS IS MODAL FOR EDIT EVENT  */}
                    <Col sm={{size: "auto", offset: 1}}>
                        <img className="icon-position2" 
                        onClick={() => modalToggleEditEvents(list.id, list.event, [list.participants], displayEventSchedule)} src={iconEdit} alt='iconEdit'/>
                          <Modal isOpen={modalEditEvents} className={className} style={{fontFamily: "Roboto"}}>
                            <ModalHeader toggle={modalToggleEditEventsDiscard} style={{height: "60px"}}>
                              <h3 className="details-title1">Edit Event</h3></ModalHeader>
                            <ModalBody>
                              <Container>
                                <Row>
                                <FormGroup>
                                  <Label className="form-spacing" for="editEventField">Event</Label>
                                  <Input className="input-spacing"
                                    type="text"
                                    name="editEventField"
                                    id="editEventField"
                                    placeholder="Enter your event name"
                                    value={indexEditEventListEvent}
                                  />
                                </FormGroup>
                                </Row>
                                <Row>
                                <FormGroup>
                                  <Label className="form-spacing" for="team">Team</Label>
                                  <Select
                                    className="form-spacing"
                                    options={optTeamData}
                                    onChange={(selected) => setFieldEvent('editTeamField', selected)}
                                    value={createEventForm.editTeamField}
                                    placeholder= "Select your team"
                                  />
                                </FormGroup>
                                </Row>
                                <Row>
                                  <ModalBody className="details-title2">
                                  Team Invited
                                  </ModalBody>
                                  <ModalBody className="team-container">
                                    <Container >
                                      <Row >

                                        {indexEditEventListParticipants.map((participant,s) => (
                                          <Col key={s} sm={{ size: 5, offset: 0 }}>
                                            <img className="icon-team-invited" src={iconTeamInvited} alt='iconTeamInvited'/> 
                                            <span style={{color:"black"}}>{participant.Name}</span>
                                          </Col>
                                        ))}

                                      </Row>
                                    </Container>
                                  </ModalBody>
                                </Row>
                                <Row>
                                <ModalBody className="details-title2">
                                  Time Slot
                                </ModalBody>
                                  <ModalBody className="time-container">
                                  <Container>

                                      {
                                        arrayDisplayEventScheduleForEditEvent
                                        .filter(events => events.eventId === parseInt(indexEditEventListId))
                                        .map(events => 
                                          <Row key={events.id}>
                                          <Col xs="auto"><img className="icon-time" src={iconSchedule2} alt='iconSchedule2'/></Col>
                                          <Col xs="auto"><span className="time-date"> {events.date}</span></Col>

                                          <Col xs="auto"><img className="icon-time" src={iconMeetingTime} alt='iconMeetingTime'/></Col><
                                            Col xs="auto"><span className="time-date">{events.startTime} - {events.endTime}</span></Col>
                                          </Row>)
                                      }

                                  </Container>
                                  </ModalBody>
                                </Row>
                              </Container>
                            </ModalBody>
                            <ModalFooter >
                              <Row >
                                <Col className="submit-pop">
                                  <Button style={{
                                    backgroundColor:"#095571", 
                                    borderColor:"#095571", 
                                    width:"100%",
                                    maxWidth:"160px",  
                                    height:"48px", 
                                    fontWeight: "bold"}} 
                                    onClick={modalToggleEditEventsDiscard} >Discard</Button>
                                </Col>
                                <Col className="submit-pop">
                                  <Button disabled style={{
                                    backgroundColor:"#EA492C", 
                                    borderColor:"#EA492C", 
                                    width:"100%",
                                    maxWidth:"160px",  
                                    height:"48px",
                                    fontWeight: "bold"}} onClick={toggleNestedEditEvents}>Save</Button>
                                </Col>
                              </Row>
                            </ModalFooter>
                            <Modal isOpen={nestedmodalEditEvents} 
                            toggle={toggleNestedEditEvents} 
                            onClosed={closeAllEditEvents ? modalToggleEditEvents : undefined} 
                            style={{
                              textAlign:"center", 
                              fontFamily: "Roboto"}}>

                              <ModalHeader style={{
                                borderBlockColor:"white", 
                                height:"50px"}}
                                toggle={modalToggleEditEvents}></ModalHeader>
                              <ModalBody style={{
                                textAlign: "center", 
                                marginBottom:"20px", 
                                marginTop:"-30px"}}><
                                  img src={missionComplete} alt='congratsEdit'/>
                                <ModalBody className="card-confirm2">Congratulations</ModalBody>
                                <ModalBody className="card-note">Your event has been edited.</ModalBody>
                                <Button style={{
                                  backgroundColor:"#095571", 
                                  borderColor:"#095571", 
                                  width:"100%",
                                  maxWidth:"160px",  
                                  height:"48px", 
                                  fontWeight: "bold"}} 
                                  onClick={toggleAllEditEvents}>Edit More</Button>
                              </ModalBody>
                            </Modal>
                          </Modal>
                        
                      {/* THIS IS MODAL FOR DELETE EVENT */}
                      <img  className= "icon-position1" 
                      onClick={() => modalToggleDelEvent(list.id)} src={iconDelete} alt='iconDelete'/>
                      </Col>

                          <Modal isOpen={modalDelEvent} 
                          className={className} 
                          style={{textAlign:"center", fontFamily: "Roboto"}}>
                              <ModalHeader style={{
                                borderBlockColor:"white", 
                                height:"50px"}}></ModalHeader>
                                <ModalBody style={{height: "400px"}}>
                                  <img src={confirmModal} alt='deleteEvent'/>
                                  <ModalBody className="card-confirm1">Delete Event</ModalBody>
                                  <ModalBody className="card-note">Are your sure to remove this event?</ModalBody> 
                                  <ModalBody className="card-note">Once you remove the event, there is no going back.</ModalBody> 
                                  <ModalBody style={{
                                    textAlign: "center", 
                                    marginBottom:"20px" }}>
                                    <Row >
                                      <Col className="submit-pop">
                                        <Button style={{
                                          backgroundColor:"#095571", 
                                          borderColor:"#095571", 
                                          width:"100%",
                                          maxWidth:"160px",  
                                          height:"48px", 
                                          fontWeight: "bold"}}
                                          onClick={modalToggleDelEvent} >Cancel</Button>
                                      </Col>
                                      <Col>
                                        <Button style={{
                                          backgroundColor:"#EA492C", 
                                          borderColor:"#EA492C", 
                                          width:"100%",
                                          maxWidth:"160px",  
                                          height:"48px",
                                          fontWeight: "bold"}} 
                                          onClick={toggleNestedDelEvent}>Delete</Button>
                                      </Col>
                                    </Row>
                                  </ModalBody>
                                </ModalBody>
                          </Modal>
                    </Row>
                  </Container>
                </CardBody>  
              )):

              // WHAT APPEARS IF THERE IS NO EVENT CREATED
                <CardBody className="card-body-content">
                  <img src={noEvents} alt='noEvents' style={{width:'50%', textAlign:'center'}}/>
                  <h4 className="oops">Oops!</h4>
                  <h5 className="input-spacing">You don't have any events.</h5>
                </CardBody>
              }
          
          </CardBody>
          </Card>
        </Col>

        </Row>
      </Container>
  
    );
};

export default HomePage;