import React, { useState } from 'react';
import { connect } from 'react-redux';
import {
    Card, CardBody, Form, FormGroup, Label, Input, Button, Container, Row, Col
  } from 'reactstrap';
import { logInAction, logOutAction } from '../redux/authentication/actions';
import { useHistory } from 'react-router-dom';
// import qs from 'qs';
import axios from 'axios';
import bgLogin1 from '../assets/bgLogin1.svg';
import bgLogin2 from '../assets/bgLogin2.svg';
import logo from '../assets/logo.svg';
import { NotificationManager} from 'react-notifications';


const LoginPage = (props) => {

    // -1- Prepare to collect login data
    const [signInData, setSignInData] = useState({
        email: '',
        password: '',
    });

    const handleChangeSignIn = (e) => {
        setSignInData({
            ...signInData,
            [e.target.name] : e.target.value
        })
    }
    // -1-

    // -2- Grab login data from API

    // This code is used for reqres.in data 
    // const [data, setData] = useState({}); 

    const handleSubmitSignIn = (e) => {
        e.preventDefault();
        
        // Axios Method
        axios({
            method: 'POST',
            url: 'https://dev-team4-backend.herokuapp.com/api/v1/auth/login',
            // url: 'https://reqres.in/api/register',
            data: JSON.stringify({
            // data: qs.stringify({
                email: signInData.email,
                password: signInData.password,
            }),
            headers: {
                'Content-Type': 'application/json',
                // 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
            }
          }).then((response) => {

            // Response from API after input login email and password
            const data = response.data;

            if ( data.data.token !== null ) {
                // Save token to local storage to keep login while page refresh (reducers.js)
                localStorage.setItem('keepToken', JSON.parse(JSON.stringify (data.data.token)));

                // Save login id to local storage for incidental need
                localStorage.setItem('keepLoginId', JSON.parse(JSON.stringify (data.data.id)));

                // Save email to local storage to be displayed on top right corner of pages (Topbar.js & Search.js)
                localStorage.setItem('keepName', JSON.parse(JSON.stringify (data.data.name)));

                // Save role to local storage to display role features
                localStorage.setItem('keepRole', JSON.parse(JSON.stringify (data.data.role)));

                // Save email to local storage to display email address
                localStorage.setItem('keepEmail', JSON.parse(JSON.stringify (data.data.email)));

                handleClick();
            } else {
                console.log('response =>', data.data);
            }
          })
          .catch(err => {
                console.clear();
                console.log('error =>', err);
                NotificationManager.error('Oops! Invalid email address or Password!','',3000);
            });
    }

    const { isLogin, logInFunc, logOutFunc } = props;
    const history = useHistory();
    // console.log('props login page =>', props);

    const handleClick = () => {
        if (isLogin) {
            logOutFunc();
            history.push('/Login');
            window.location.reload();
        } else {
            logInFunc();

            // Visitted page if user logged in successfully
            history.push('/');

            // Refresh page to display email on top right corner of page (Topbar.js & Search.js)
            window.location.reload();
        }
    }
    // -2-
    
    // -3- Logout function
    const handleSignOut = () => {
        logOutFunc();

        // Clear all config after logout
        localStorage.clear();

        history.push('/');
        window.location.reload();
    }
    // -3-

    // -4- Internal CSS Style
    const myInput = {
        width: '100%',
        maxWidth: '16em',
        backgroundColor: 'transparent',
        color: '#fff'
    }
    // -4-


    return (
        <Container fluid style={{width: '100%'}}>
            <Row>
                <Col style={{backgroundColor:"#EBEBEB"}}>
                    <img src={bgLogin1} alt='bgLogin1' style={{ width: '100%', marginTop: "8%"}}/>
                </Col>
                <Col md={4} style={{backgroundImage: `url(${bgLogin2})`, height: '100vh'}}>
                <center>
                    <Card style={{ width: '100%', maxWidth: '18rem', backgroundColor: '#095571', border: 'none', marginTop: '25%'}}>
                    <CardBody style={{textAlign: "center"}}>
                    <img src={logo} alt='logo.svg' className="App-logo"/><p/>
                    <Form style={{color: '#fff'}}>
                        <FormGroup style={{textAlign: 'left'}}>
                            <div>
                                {isLogin ? 'Hello '+ (localStorage.getItem('keepName')) +'. You are logged in by using ' + (localStorage.getItem('keepEmail')) + ' as ' + (localStorage.getItem('keepRole')) + ' ' : null}
                            </div>
                            <div style={{fontSize: 'x-large'}}>
                                {isLogin ? null : 'Welcome!'}
                            </div>
                            <p style={{fontSize: 'medium'}}>
                                {isLogin ? null : 'Sign in to manage your schedule and team.'}
                            </p>
                        </FormGroup>

                        <FormGroup style={{textAlign: 'left'}}>
                            <Label>
                                {isLogin ? null : 'Email address'}
                            </Label>
                            <Input 
                                type={isLogin ? 'hidden' : 'email'}
                                name="email" // Authentication Key
                                className="form-control Input" 
                                style={myInput} 
                                placeholder="Enter your email"
                                onChange={handleChangeSignIn}
                            />
                        </FormGroup>

                        <FormGroup style={{textAlign: 'left'}}>
                            <Label>
                                {isLogin ? null : 'Password'}
                            </Label>
                            <Input 
                                type={isLogin ? 'hidden' : 'password'}
                                name="password" // Authentication Key
                                className="form-control" 
                                style={myInput} 
                                placeholder="Enter your password"
                                onChange={handleChangeSignIn}
                            />
                        </FormGroup>
                        <p style={{textAlign: 'right'}}>
                            <a href="/ForgotPass" style={{color: '#fff', textDecoration: 'none', fontSize: 'small'}}>
                                {isLogin ? null : 'Forgot password?'}
                             </a>
                        </p>

                        <Button 
                            onClick={isLogin ? handleSignOut : handleSubmitSignIn}
                            style={{backgroundColor: '#F9CC2C', color: 'black', width: '16rem'}}
                            type="submit">
                                {isLogin ? 'Sign out' : 'Sign in'}
                        </Button>

                        <p style={{textAlign: 'center', fontSize: 'small'}}>
                            {isLogin ? null : `Don't have an account?`}
                            <a href="/Register" style={{color: '#F9CC2C', textDecoration: 'none'}}>
                                <b>{isLogin ? null : ' Sign up'}</b>
                            </a>
                        </p>

                    </Form>
                    </CardBody>
                    </Card>
                </center>
                </Col>
            </Row>
        </Container>

    );
};

const mapStateToProps = (state) => ({
    isLogin: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
    logInFunc: () => dispatch(logInAction),
    logOutFunc: () => dispatch(logOutAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);