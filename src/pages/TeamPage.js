import React, { useState,useEffect } from 'react';
import { 
    Card, 
    CardTitle, 
    CardText,
    Container, 
    Row, 
    Col, 
    Button, 
    Form, 
    FormGroup, 
    Label, Input, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    ModalFooter, 
    // ButtonDropdown, 
    DropdownToggle, 
    DropdownMenu, 
    DropdownItem, 
    UncontrolledDropdown
   } from 'reactstrap';
import Select from 'react-select';
import '../assets/css/teampage.css';
import '../assets/css/homepage.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faEllipsisV} from "@fortawesome/free-solid-svg-icons";
import teamPage from '../assets/TeamPage.svg';
import confirmModal from "../assets/confirmModal.svg";
import missionComplete from "../assets/missionComplete.svg";
import fotoNuri from '../assets/FotoNuri.svg';
import noEvents from "../assets/noEvents.svg";
import axios from 'axios';
import { NotificationManager } from 'react-notifications';
// import teamIcon from '../assets/icons/teamIcon.svg'
import deleteTeamMember from '../assets/iconDeleteTeamMember.svg';


const TeamPage = (props) => {

        // -1- Grab Innovator data
        const [innovatorsData, setInnovatorsData] = useState([]);
        const optInnovatorsData = innovatorsData?.reduce((acc, data) => {
            acc.push({
                label: data.name,
                value: data.id,
            });
            return acc;
        }, []);

        useEffect(() => {
            const fetchData = async () => {
                axios({
                    method: 'GET',
                    url: 'https://dev-team4-backend.herokuapp.com/api/v1/user/innovator',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                    }
                }).then((response) => {
                    console.log('response for innovator data =>', response.data);
                    setInnovatorsData(response.data.data);
                })
                .catch(err => {
                    console.log('error =>', err);
                    NotificationManager.error(err,'',3000);
                });
            }
            fetchData();
        }, []);
        // -1-


        // -2- General Modal Function
        const [isOpenModal, setIsOpenModal] = useState(false);
        const [isOpenSuccModal, setIsOpenSuccModal] = useState(false);
        const [closeAll, setCloseAll] = useState(false);
        
        const {
            buttonLabel,
            className
        } = props;          
        
        const toggle = () => setIsOpenModal(!isOpenModal);
        const toggleNested = () => {
            setIsOpenSuccModal(!isOpenSuccModal);
            setCloseAll(false);
        }
        const toggleAll = () => {
            setIsOpenSuccModal(!isOpenSuccModal);
            setCloseAll(true);
        }
        
        const closeBtn = <button className="close" onClick={toggle}>&times;</button>;
        // -2-


        // -3- Prepare to create Team data
        const userId = [];
        const [createTeamForm, setCreateTeamForm] = useState({
            name: '',
            members: [
                {userId: userId}
            ]
        });

        const setFieldTeam = (field, value) => {
            setCreateTeamForm({
            ...createTeamForm,
            [field]: value,
            });
        }

        const handleCreate = () => {
            setIsOpenModal(true);
        }
        // -3-


        // -4- Create Team data to database by API
        const submitCreateTeam = (e) => {
            e.preventDefault();
            setIsOpenModal(false);

            if (submitCreateTeam) {
                if ((!createTeamForm.userId3 && !createTeamForm.userId2 && !createTeamForm.userId1) || (!createTeamForm.userId3 && !createTeamForm.userId1) || (!createTeamForm.userId2 && !createTeamForm.userId1)) {
                    toggle();
                  }
                else if (!createTeamForm.userId3 && !createTeamForm.userId2) {
                    axios({
                        method: 'POST',
                        url: 'https://dev-team4-backend.herokuapp.com/api/v1/team/',
                        data: JSON.stringify({
                            name: createTeamForm.name,
                            members: [
                                {userId: createTeamForm.userId1.value}
                            ]
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                        }
                    }).then((response) => {
                            console.log('response for create team submission =>', response);
                            const res = response.data;
                            if (res) {
                                console.log('cek res');
                                setIsOpenSuccModal(true);
                            }
                            NotificationManager.info('S U C C E S S !','',3000);
                            window.location.reload();
                    })
                    .catch(err => {
                        console.log('error =>', err);
                        NotificationManager.error(err,'Fail to save your data',3000);
                        toggle();
                    });
                }
                else if (!createTeamForm.userId3) {
                    axios({
                        method: 'POST',
                        url: 'https://dev-team4-backend.herokuapp.com/api/v1/team/',
                        data: JSON.stringify({
                            name: createTeamForm.name,
                            members: [
                                {userId: createTeamForm.userId1.value},
                                {userId: createTeamForm.userId2.value}
                            ]
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                        }
                    }).then((response) => {
                            console.log('response for create team submission =>', response);
                            const res = response.data;
                            if (res) {
                                console.log('cek res');
                                setIsOpenSuccModal(true);
                            }
                            NotificationManager.info('S U C C E S S !','',3000);
                            window.location.reload();
                    })
                    .catch(err => {
                        console.log('error =>', err);
                        NotificationManager.error(err,'Fail to save your data',3000);
                        toggle();
                    });
                }
                else {
                    axios({
                        method: 'POST',
                        url: 'https://dev-team4-backend.herokuapp.com/api/v1/team/',
                        data: JSON.stringify({
                            name: createTeamForm.name,
                            members: [
                                {userId: createTeamForm.userId1.value},
                                {userId: createTeamForm.userId2.value},
                                {userId: createTeamForm.userId3.value}
                            ]
                        }),
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                        }
                    }).then((response) => {
                            console.log('response for create team submission =>', response);
                            const res = response.data;
                            if (res) {
                                console.log('cek res');
                                setIsOpenSuccModal(true);
                            }
                            NotificationManager.info('S U C C E S S !','',3000);
                            window.location.reload();
                    })
                    .catch(err => {
                        console.log('error =>', err);
                        NotificationManager.error(err,'Fail to save your data',3000);
                        toggle();
                    });
                }
            }
        }

        console.log('createTeamForm =>', createTeamForm);
        // -4-


        // -5- DROPDOWN FUNCTION for YOUR TEAM DISPLAY
        // const [indexEditTeamListId, setIndexEditTeamListId] = useState();
        const [indexEditTeamListTeam, setIndexEditTeamListTeam] = useState();
        const [indexEditTeamListMembers, setIndexEditTeamListMembers] = useState([]);
        
        const [indexDelTeam, setIndexDelTeam] = useState({});

            // -5.1- MODAL FUNCTION to EDIT TEAM
            const [modalEditTeam, setmodalEditTeam] = useState(false);
            const modalToggleEditTeam = (indexEditTeamListId, indexEditTeamListTeam, [indexEditTeamListMembers]) => {
                setmodalEditTeam(!modalEditTeam);
                // setIndexEditTeamListId(indexEditTeamListId)
                setIndexEditTeamListTeam(indexEditTeamListTeam);
                setIndexEditTeamListMembers(indexEditTeamListMembers);

                console.log('MEMBERNYA NIH =>', indexEditTeamListMembers);
            }

            const modalToggleEditTeamCancel = () => {
                setmodalEditTeam(!modalEditTeam);
                window.location.reload();
            }
            // -5.1-

            // -5.2- MODAL FUNCTION to DELETE TEAM
            const [modalDelTeam, setmodalDelTeam] = useState(false);
            const modalToggleDelTeam = (indexDelTeam) => {
                setmodalDelTeam(!modalDelTeam);
                setIndexDelTeam(indexDelTeam);
            }
            // -5.2-


        // -6- Display data in Your Team column
        const [displayTeam, setDisplayTeam] = useState([]);

        useEffect(() => {
            const fetchDataYourTeam = async () => {
                axios({
                    method: 'GET',
                    url: 'https://dev-team4-backend.herokuapp.com/api/v1/team',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                    }
                }).then((response) => {
                    console.log('response for your team column =>', response.data.data);
                    setDisplayTeam(response.data.data);
                })
                .catch(err => {
                    console.log('error =>', err);
                    NotificationManager.error(err,'',3000);
                });
            }
            fetchDataYourTeam();
        }, []);
        // -6-

        
        // -7- Delete Team
        const handleDeleteTeam = (id) => {
            const url = `https://dev-team4-backend.herokuapp.com/api/v1/team/${id}`;

            axios({
                method: 'DELETE',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                    console.log('response for your team deletion =>', response.data.data);
                    window.location.reload();
            })
            .catch(err => {
                    console.log('error =>', err);
            });
        }

        // const testDelete = (dId) => {
        //     console.log('Test Delete =>', dId)
        // };
        // -7-


        // -8- Handle Edit Team
        // -----
        const optInnovatorsDataInEditTeam = innovatorsData?.reduce((acc, data) => {
            acc.push({
                label: data.name,
                value: {
                    id: data.id, 
                    name: data.name, 
                    email: data.email, 
                    role: data.role
                }
            });
            return acc;
        }, []);

        const [editTeamForm, setEditTeamForm] = useState({});

        const addMemberListInEditTeam = (field, selected) => {
            setEditTeamForm({
                ...editTeamForm,
                [field]: selected
                });

                setIndexEditTeamListMembers(indexEditTeamListMembers.concat(selected.value));
                console.log('MEMBER SETELAH DIUPDATE =>', indexEditTeamListMembers);
        }
        // -----

        const delMemberListInEditTeam = (indexDelMemberListInEditTeam) => {
            setIndexEditTeamListMembers(indexEditTeamListMembers.splice(indexDelMemberListInEditTeam, 1));

            const newIndexEditTeamListMembers = [...indexEditTeamListMembers];
            setIndexEditTeamListMembers(newIndexEditTeamListMembers);

            console.log('MEMBER SETELAH DIHAPUS =>', indexEditTeamListMembers);
        }

        // const saveEditTeam = async (e) => {
        //     e.preventDefault();
        //     setIsOpenModal(false);

        //     axios({
        //         method: 'PUT',
        //         url: 'https://dev-team4-backend.herokuapp.com/api/v1/team/',
        //         data: JSON.stringify({
        //             name: createTeamForm.name,
        //             members: [
        //                 {userId: createTeamForm.userId2.value},
        //             ]
        //         }),
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
        //         }
        //     }).then((response) => {
        //             console.log('response for create team submission =>', response);
        //             const res = response.data;
        //             if (res) {
        //                 console.log('cek res');
        //                 setIsOpenSuccModal(true);
        //             }
        //             window.location.reload();
        //     })
        //     .catch(err => {
        //         console.log('error =>', err);
        //         NotificationManager.error(err,'Fail to save your data',3000);
        //     });
        // }
        // -8-

        
        return (
            
            <Container fluid className="container-whole">
                <Row className="template">

                {/* ============================= */}
                {/* CREATE TEAM */}
                {/* ============================= */}

                <Col sm={{ size: 6, offset: 0}} className="col-spacing">
                    {/* UNTUK GAMBAR */}
                    <div style={{ width:'110%'}}>
                        <img classname="Gambar" src={teamPage} alt='teamPage' style={{ width: '100%'}}/>
                    </div>

                    {/* Untuk form */}
                    <div style={{width:'100%'}}>
                    <Card style={{height: '480px', marginTop: "-30px"}}>
                    <Form style={{padding: '20px'}}>
                    <h3 className="Judul">Create Team</h3>
                    <CardText style={{padding: "10px 0px 0px 20px"}}>
                        <Form>
                            <FormGroup>
                                <Label for="exampleEmail" style={{
                                    color: '#002C46', 
                                    fontSize: "14px"
                                    }}>
                                        Team Name
                                </Label>
                                <Input 
                                    className="form-spacing"
                                    type="email" 
                                    name="name" 
                                    id="name" 
                                    placeholder="Enter your team name" 
                                    onChange={(e) => setFieldTeam('name', e.target.value)}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect"style={{ 
                                    color: '#002C46', 
                                    fontSize: "14px"
                                    }}>
                                        Member 1
                                </Label>
                                    <Select
                                        className="form-spacing"
                                        options={optInnovatorsData}
                                        onChange={(selected) => setFieldTeam('userId1', selected)}
                                        value={createTeamForm.userId1}
                                    />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect" style={{
                                    fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif", 
                                    color: '#002C46', 
                                    fontSize: "14px"
                                    }}>
                                        Member 2
                                </Label>
                                <Select
                                        className="form-spacing"
                                        options={optInnovatorsData}
                                        onChange={(selected) => setFieldTeam('userId2', selected)}
                                        value={createTeamForm.userId2}
                                />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleSelect" style={{
                                    fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif", 
                                    color: '#002C46', 
                                    fontSize: "14px"
                                    }}>
                                        Member 3
                                </Label>
                                <Select
                                    className="form-spacing"
                                    options={optInnovatorsData}
                                    onChange={(selected) => setFieldTeam('userId3', selected)}
                                    value={createTeamForm.userId3}
                                />
                            </FormGroup>
                        </Form>
                    </CardText>

                    <div>
                        <Button classname="button1" style={{
                            backgroundColor:"#095571", 
                            borderColor: "#EBEBEB",
                            color:"#fff", 
                            float:"right",
                            marginTop:'25px', 
                            width: "100%", 
                            maxWidth: "160px",  
                            marginBottom:'38px'
                            }} 
                            disabled={createTeamForm.name.length > 0 ? false : true}
                            onClick={handleCreate}>
                            Create{buttonLabel}
                        </Button>
                        <Modal isOpen={isOpenModal} toggle={toggle} className={className} style={{
                            textAlign:"center", 
                            fontFamily: "Roboto"
                            }}>
                        
                            <ModalHeader toggle={toggleAll} close={closeBtn} style={{
                                borderBlockColor:"white", 
                                height:"50px"
                                }}>
                            </ModalHeader>
                            <ModalBody style={{height: "400px"}}>
                                <img src={confirmModal} alt='submitTeam'/>
                                <ModalBody className="card-confirm1">Submit Team</ModalBody>
                                <ModalBody className="card-note">Are you sure to create this team?</ModalBody>
                                <ModalBody style={{textAlign: "center", marginBottom:"20px" }}>
                                    <Row >
                                        <Col className="submit-pop">
                                            <Button style={{
                                                backgroundColor:"#095571", 
                                                borderColor:"#095571", 
                                                width: "100%", 
                                                maxWidth: "160px", 
                                                height:"48px", 
                                                fontWeight: "bold"
                                                }}
                                                onClick={toggle} >
                                                    Cancel
                                            </Button>
                                        </Col>
                                        <Col>
                                            <Button style={{
                                                backgroundColor:"#EA492C", 
                                                borderColor:"#EA492C", 
                                                width: "100%", 
                                                maxWidth: "160px", 
                                                height:"48px",
                                                fontWeight: "bold"
                                                }} 
                                                onClick={submitCreateTeam}>
                                                    Create
                                            </Button>
                                        </Col>
                                    </Row>
                            </ModalBody>
                            </ModalBody>
                                <Modal style={{textAlign:"center"}} isOpen={isOpenSuccModal} toggle={toggleNested} onClosed={closeAll ? toggle : undefined} >
                                    <ModalHeader 
                                        toggle={toggleAll}
                                        close={closeBtn} 
                                        style={{borderBlockColor:"white"}} >
                                    </ModalHeader>
                                    <ModalBody style={{fontFamily: 'Roboto'}}>
                                        <img src={missionComplete} style={{width:'50%'}} alt='congratulation.svg'/>
                                        <ModalBody  className="card-confirm1">Mission Complete!</ModalBody>
                                        <ModalBody className="card-note"> Your team has been created</ModalBody>
                                        <Button style={{
                                            backgroundColor:"#095571", 
                                            borderColor:"#095571", 
                                            width: "100%", 
                                            maxWidth: "160px", 
                                            height:"48px", 
                                            fontWeight: "bold"
                                            }} 
                                            onClick={toggleAll}>
                                                Back to Home
                                        </Button>
                                    </ModalBody>
                                
                                </Modal>
                        </Modal>
                    </div>
                    <br />
                    
                    </Form>
                    </Card>
                    </div>
                </Col>


                {/* ============================= */}
 
 
                {/* YOUR TEAM */}
                {/* ============================= */}

                <Col sm={{ size: 6, offset: 0.5}} className="col-spacing">
                    <Card style={{
                        backgroundColor:'#fff', 
                        height:'100%', 
                        padding: "20px", 
                        overflowY:'scroll', 
                        maxHeight: '683px'
                        }}>
                        <h3 className='Judul' >Your Team</h3>
                        <Row>
                        <Col style={{width:'100%', float:'left'}}>

                        {/* ----- Display Your Team Begin Code ----- */}
                        {displayTeam.length > 0 ? 
                        displayTeam.map((list, i) => (

                            <Card body outline style={{
                                outlineColor: 'grey', 
                                backgroundColor:'#FFFFFF', 
                                width:'220px', 
                                float:'left', 
                                margin:'20px'
                                }} 
                                key={list.id}>

                                <Row>
                                    <Col sm={{ size: 9, offset: 0}}>
                                    <CardTitle tag="h6" style={{fontSize:'15px'}} ><b>{list.team}</b></CardTitle>
                                    </Col>

                                    <Col sm={{ size: 3, offset: 0}}>
                                    <UncontrolledDropdown  style={{
                                        float:'right', 
                                        backgroundImage:'../assets/TitikTiga.png', 
                                        backgroundColor:'white', border:'0px'}}>
                                   
                                        <DropdownToggle style={{
                                            border:'0px', 
                                            borderRadius:'0px', 
                                            backgroundColor:'transparent'
                                            }}>
                                            <FontAwesomeIcon icon={faEllipsisV} style={{color:'black'}}/>
                                        </DropdownToggle>
                                        <DropdownMenu>
                                            <DropdownItem onClick={() => modalToggleEditTeam(list.id, list.team, [list.members])}>Edit team</DropdownItem>
                                            <Modal isOpen={modalEditTeam} className={className} style={{fontFamily: "Roboto"}}>
                                                <ModalHeader onClick={modalToggleEditTeamCancel} close={closeBtn} style={{height:"50px"}}>{indexEditTeamListTeam}</ModalHeader>
                                                <ModalBody>
                                                    <Container>
                                                        <Row>
                                                            <FormGroup>
                                                                <Label className="form-spacing" for="Event">Team name</Label>
                                                                <Input className="input-spacing"
                                                                    type="text"
                                                                    name="name"
                                                                    value={indexEditTeamListTeam}
                                                                />
                                                            </FormGroup>
                                                        </Row>
                                                        <Row>
                                                            <FormGroup>
                                                                <Row className="align-items-center border-white">
                                                                    <Label className="form-spacing" for="team">Member</Label>
                                                                </Row>

                                                                <Row>
                                                                    <Col>
                                                                        <Select
                                                                            className="form-spacing"
                                                                            options={optInnovatorsDataInEditTeam}
                                                                            onChange={(selected) => addMemberListInEditTeam('updateUserId', selected)}
                                                                            value={editTeamForm.updateUserId}
                                                                        />
                                                                    </Col>
                                                                </Row>

                                                                {indexEditTeamListMembers.map((memberList, k) => (
                                                                    <Row key={k} className="align-items-center border-white" style={{backgroundColor:"#EBEBEB", borderLeftColor:"#fff", margin:"1px"}}>
                                                                        <Col xs="auto">
                                                                            <img top width="100%" src={fotoNuri} alt="Card cap" /></Col>
                                                                        <Col>{memberList.name}</Col>
                                                                        <Col xs="auto"><img type="Button" onClick={() => delMemberListInEditTeam(k)} src={deleteTeamMember} alt="iconDeleteTeamMember" /></Col>
                                                                    </Row>
                                                                ))}

                                                            </FormGroup>
                                                        </Row><br />
                                                    </Container>
                                                </ModalBody>
                                                <ModalFooter >
                                                <Row >
                                                    <Col className="submit-pop">
                                                        <Button style={{
                                                            backgroundColor:"#095571", 
                                                            borderColor:"#095571", 
                                                            width: "100%", 
                                                            maxWidth: "160px", 
                                                            height:"48px", 
                                                            fontWeight: "bold"
                                                            }} 
                                                            onClick={modalToggleEditTeamCancel} >
                                                                Cancel
                                                        </Button>
                                                    </Col>
                                                    <Col>
                                                        <Button disabled style={{
                                                            backgroundColor:"#EA492C", 
                                                            borderColor:"#EA492C", 
                                                            width: "100%", 
                                                            maxWidth: "160px", 
                                                            height:"48px",
                                                            fontWeight: "bold"
                                                            }} 
                                                            onClick={'/'}>Save</Button>
                                                    </Col>
                                                </Row>
                                                </ModalFooter>
                                            </Modal>

                                            <DropdownItem onClick={() => modalToggleDelTeam(list.id)} >Delete team</DropdownItem>
                                                <Modal isOpen={modalDelTeam} className={className} style={{textAlign:"center"}}>
                                                    <ModalHeader onClick={modalToggleEditTeamCancel} close={closeBtn} style={{borderBlockColor:"white", height:"50px"}}></ModalHeader>
                                                    <ModalBody>
                                                        <img src={confirmModal} alt='deleteTeam'/>
                                                        <ModalBody className="card-confirm1">Delete Team</ModalBody>
                                                        <ModalBody className="card-note">Are you sure to remove this team?</ModalBody> 
                                                        <ModalBody style={{textAlign: "center", marginBottom:"20px" }}>
                                                            <Row >
                                                            <Col className="submit-pop">
                                                                <Button style={{
                                                                    backgroundColor:"#095571", 
                                                                    borderColor:"#095571", 
                                                                    width: "100%", 
                                                                    maxWidth: "160px", 
                                                                    height:"48px", 
                                                                    fontWeight: "bold"
                                                                    }} 
                                                                    onClick={modalToggleDelTeam} >
                                                                        Cancel
                                                                </Button>
                                                            </Col>
                                                            <Col>
                                                                <Button style={{
                                                                    backgroundColor:"#EA492C", 
                                                                    borderColor:"#EA492C", 
                                                                    width: "100%", 
                                                                    maxWidth: "160px", 
                                                                    height:"48px",
                                                                    fontWeight: "bold"
                                                                    }} 
                                                                    onClick={() => handleDeleteTeam(indexDelTeam)}>
                                                                        Delete
                                                                </Button>
                                                            </Col>
                                                            </Row>
                                                        </ModalBody>
                                                    </ModalBody>
                                                </Modal>
                                        </DropdownMenu>
                                    </UncontrolledDropdown>
                                    {/* </ButtonDropdown> */}
                                    </Col>
                                </Row>

                                <Row>

                                    {list.members.map((member, j) => (
                                        <Row key={j}>
                                            <img src={fotoNuri} style={{
                                                borderRadius:'100px', 
                                                width:'50px'
                                                }}
                                                alt='fotonuri'>
                                            </img>
                                             {member.name}
                                        </Row>
                                    ))}

                                </Row>
                               
                            </Card>
                        )):
                        // ----- Display Your Team End Code -----

                        // WHAT APPEARS IF THERE IS NO EVENT CREATED
                            <Card className="card-body-content" style={{
                                alignItems:'center', 
                                verticalAlign:'center', 
                                height:'580px'}}>
                                <img src={noEvents} alt='noEvents' style={{
                                    width:'50%', 
                                    outlineStyle:'none !important'}}/>
                                <h4 className="oops">Oops!</h4>
                                <h5 className="input-spacing">You don't have any events.</h5>
                            </Card>
                            
                        }

                        </Col>
                        </Row>
                    </Card>
                </Col>

              </Row>
            </Container>
          );
};

export default TeamPage;