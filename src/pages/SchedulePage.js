import React, {useState, useEffect} from 'react';
import "../assets/css/schedulepage.css";
import "../assets/css/homepage.css";
import { 
    Container, 
    Card, 
    CardBody, 
    Table, 
    Button, 
    Modal, 
    ModalHeader, 
    ModalBody, 
    Row, 
    Input,
    Pagination, 
    PaginationItem, 
    PaginationLink, 
    Col} from 'reactstrap';
import iconDownload from "../assets/iconDownload.svg";
import deleteComplete from "../assets/deleteComplete.svg";
import confirmModal from "../assets/confirmModal.svg";
import axios from 'axios';
// import Pagination from 'pagination-react-hooks';


const SchedulePage = (props) => {
    const {
        buttonLabel,
        className
    } = props;


    // -1- Get Schedule Data
    const [scheduleData, setScheduleData] = useState([]);

    useEffect(() => {
        const fetchScheduleData = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/schedule/detail',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for schedule data =>', response.data.data);
                setScheduleData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchScheduleData();
    }, []);
    // -1-


    // MODAL FUNCTION FOR MARKING SCHEDULE AS DONE //
    const [modalDoneSchedule, setmodalDoneSchedule] = useState(false);
    const [nestedmodalDoneSchedule, setNestedmodalDoneSchedule] = useState(false);
    const [closeAllDoneSchedule, setcloseAllDoneSchedule] = useState(false);

    const modalToggleDoneSchedule = () => setmodalDoneSchedule(!modalDoneSchedule);
    const toggleNestedDoneSchedule= () => {
        setNestedmodalDoneSchedule(!nestedmodalDoneSchedule);
        setcloseAllDoneSchedule(false);
    }
    const toggleAllDoneSchedule = () => {
        setNestedmodalDoneSchedule(!nestedmodalDoneSchedule);
        setcloseAllDoneSchedule(true); 
    }


    // PAGE STYLE
    return (

        <Container fluid className="container-whole"> 
        <Card className="schedule-card">
            <CardBody >
                <h3 className="card-headings">Your Schedule</h3>
                <Container style={{padding: "20px"}}>

                <Table bordered responsive hover >
                    <thead className="thead-schedule">
                        <tr>
                        <th>Team Name</th>
                        <th>Schedule</th>
                        <th>Event Name</th>
                        <th>Download</th>
                        <th>Status</th>
                        </tr>
                    </thead>
                    <tbody className="tbody-font">

                        {/* ----- Display Schedule Begin Code ----- */}
                        {scheduleData.map((list, i) => (

                            <tr>
                            <td>{list.teamName}</td>
                            <td>{list.date}, {list.startTime}</td>
                            <td>{list.name}</td>
                            <td className="tbody-center">
                                {list.links.map((link, j) => (
                                    <a href={link.download}><img className="tbody-center" src={iconDownload} alt='download'/></a>
                                ))}
                            </td>
                            <td className="tbody-center">
                                <Button onClick={modalToggleDoneSchedule} 
                                style={{
                                    fontSize: "10px", 
                                    backgroundColor: "#EBEBEB", 
                                    color: "#979797", 
                                    borderColor:"#EBEBEB" }}>
                                Done{buttonLabel}</Button></td>
                            </tr>

                        ))}
                        {/* ----- Display Schedule End Code ----- */}

                    </tbody>
                </Table>

                {/* PAGINATION CODES START HERE */}
                <Container style={{
                    float: "right", 
                    width: "31.6%", 
                    fontSize: "14px", 
                    marginTop: "3%"}}>
                    <Row>
                        <Col sm={{size: "auto" }} >Page</Col>
                        <Col sm={{size: "auto"}}>
                        <Input style={{
                            width: "80px", 
                            height: "25px", 
                            fontSize: "14px", 
                            textAlign: "center"}}
                        type="text"/>
                        </Col >
                        <Col sm={{size: "auto"}}> of 16
                        </Col>
                        <Col sm={{size: "auto"}}>
                            <Pagination size="md" label="pagination-schedule">
                                <PaginationItem>
                                    <PaginationLink previous href="#" className="page-link" style={{
                                        backgroundColor: "#095571", 
                                        height: "25px", 
                                        width: "32px", 
                                        color: "#FFFFFF", 
                                        fontSize: "17px", 
                                        padding: "0px 5px 0px 5px", 
                                        textAlign: "center", 
                                        fontWeight: "bolder", 
                                        borderRadius: "5px"}}/>
                                </PaginationItem>
                                <PaginationItem>
                                <PaginationLink next href="#" style={{
                                    backgroundColor: "#095571", 
                                    height: "25px", 
                                    width: "32px", 
                                    color: "#FFFFFF", 
                                    fontSize: "17px", 
                                    padding: "0px 5px 0px 5px", 
                                    textAlign: "center", 
                                    fontWeight: "bolder", 
                                    borderRadius: "5px"}} />
                                </PaginationItem>
                            </Pagination>
                        </Col>
                    </Row>
                </Container>
                   
                </Container>
            </CardBody>
        </Card>

            {/* MODAL FOR MARKING SCHEDULE AS DONE */}
            <Modal isOpen={modalDoneSchedule} 
            toggle={modalToggleDoneSchedule} 
            className={className} 
            style={{
                textAlign:"center", 
                fontFamily: 'Roboto'}}>

                <ModalHeader style={{
                    borderBlockColor:"white", 
                    height:"50px"}}
                    toggle={modalToggleDoneSchedule}></ModalHeader>
                    
                <ModalBody ><img src={confirmModal} alt='EventMarkedasDone'/>
                    <ModalBody className="card-confirm1">Mark as Done</ModalBody>
                    <ModalBody className="card-note">Are your sure to mark this schedule as done?</ModalBody> 
                    <ModalBody style={{
                        textAlign: "center", 
                        marginBottom:"20px" }}>
                    <Row >
                        <Col className="submit-pop">
                        <Button style={{
                            backgroundColor:"#095571", 
                            borderColor:"#095571", 
                            width:"160px", 
                            height:"48px", 
                            fontWeight: "bold"}}
                            onClick={modalToggleDoneSchedule} >Cancel</Button>
                        </Col>
                        <Col>
                        <Button style={{
                            backgroundColor:"#45A35D", 
                            borderColor:"#45A35D", 
                            width:"160px", 
                            height:"48px",
                            fontWeight: "bold"}} 
                            onClick={toggleNestedDoneSchedule}>Mark as Done</Button>
                        </Col>
                    </Row>
                    </ModalBody>
                    <Modal isOpen={nestedmodalDoneSchedule} 
                    toggle={toggleNestedDoneSchedule} 
                    onClosed={closeAllDoneSchedule ? modalToggleDoneSchedule : undefined} 
                    style={{
                        textAlign:"center",
                        fontFamily: 'Roboto'}}>
                        <ModalHeader style={{
                            borderBlockColor:"white", 
                            height:"50px"}}
                            toggle={modalToggleDoneSchedule}></ModalHeader>
                        <ModalBody style={{
                            textAlign: "center", 
                            height: "400px", 
                            paddingTop:"30px"}}>
                                <img src={deleteComplete} alt='ScheduleMarkedasDone'/>
                        <ModalBody className="card-confirm2">Mission Complete</ModalBody>
                        <ModalBody className="card-note">Your schedule has been marked as done.</ModalBody>
                        <Button style={{
                            backgroundColor:"#095571", 
                            borderColor:"#095571", 
                            width:"160px", 
                            height:"48px", 
                            fontWeight: "bold"}} 
                            onClick={toggleAllDoneSchedule}>Back to Schedule</Button>
                        </ModalBody>
                    </Modal>
                </ModalBody>
            </Modal>
            
        </Container>
    );

}

export default SchedulePage;