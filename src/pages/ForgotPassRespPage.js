import React from 'react';
import {
    Card, CardBody, Form, FormGroup, Container, Row, Col
  } from 'reactstrap';
import bgForgotPass2 from '../assets/bgForgotPass2.svg';
import bgLogin2 from '../assets/bgLogin2.svg';
import logo from '../assets/logo.svg';


const ForgotPassRespPage = () => {

    return (

        <Container fluid style={{width: '100%'}}>
        <Row>
            <Col style={{backgroundColor:"#EBEBEB"}}>
                <img src={bgForgotPass2} alt='bgForgotPass2.svg' 
                style={{ 
                    width: '100%', 
                    marginTop: "8%" }}/>
            </Col>
            <Col md={4} style={{
                backgroundImage: `url(${bgLogin2})`, 
                height: '100vh'}}>
            <center>
                <Card style={{ 
                    width: '100%', 
                    maxWidth: '18rem', 
                    backgroundColor: '#095571', 
                    border: 'none', 
                    marginTop: '35%' }}>
                <CardBody>
                <img src={logo} alt='logo.svg' className="App-logo"/><p/>
                <Form style={{color: '#fff'}}>
                    <FormGroup style={{textAlign: 'left'}}>
                        <div style={{fontSize: 'x-large'}}>
                            Email has been sent!
                        </div>
                        <p style={{fontSize: 'medium'}}>
                            Please check your inbox and click in the received link to reset password.
                        </p>
                    </FormGroup>
                     
                    <p style={{
                        textAlign: 'left', 
                        fontSize: 'medium'}}>
                        Didn’t receive the link?
                        <a href="/ForgotPass" style={{
                            color: '#F9CC2C', 
                            textDecoration: 'none'}}>
                            <b> Resend</b>
                        </a><br />
                        or back to
                        <a href="/" style={{
                            color: '#F9CC2C', 
                            textDecoration: 'none'}}>
                            <b> Sign in</b>
                        </a>
                    </p>

                </Form>
                </CardBody>
                </Card>
            </center>
            </Col>
        </Row>
    </Container>
        
    );

}

export default ForgotPassRespPage;