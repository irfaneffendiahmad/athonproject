import React, {useState, useEffect} from 'react';
import { Badge, Card, CardTitle, Container, Row, Col, Button, Input, Modal, ModalHeader, ModalBody, ModalFooter, CardBody, } from 'reactstrap';
import "../assets/css/notificationpage.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faCheck, faTimes, faClock} from "@fortawesome/free-solid-svg-icons";
import download from "../assets/download.svg";
import axios from 'axios';


const NotificationPage = (props) => {

    const [createForm, setCreateForm] = useState({
        isApproved: '',
        note: ''
    });

    const setFieldForm = (field, value) => {
        setCreateForm({
        ...createForm,
        [field]: value,
        });
    }


    // -1- Get Request Data
    const [requestData, setRequestData] = useState([]);
    const [requestCount, setRequestCount] = useState(6);

    useEffect(() => {
        const fetchRequestData = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/notification/innovation-manager/request',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for request notification data =>', response.data.data);
                setRequestData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchRequestData();
    }, []);
    // -1-


    // -2- Get Activity Data
    const [activityData, setActivityData] = useState([]);
    const [activityCount, setActivityCount] = useState(8);

    useEffect(() => {
        const fetchActivityData = async () => {
            axios({
                method: 'GET',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/notification/activity',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                }
            }).then((response) => {
                console.log('response for activity notification data =>', response.data.data);
                setActivityData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchActivityData();
    }, []);
    // -2-


    // -3- Request Approval
    const approveRequest = (id) => {
        const url = `https://dev-team4-backend.herokuapp.com/api/v1/notification/innovation-manager/response/${id}`;

        axios({
            method: 'POST',
            url: url,
            data: JSON.stringify({
                isApproved: true,
                note: createForm.note
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
                console.log('response for your approval =>', response.data.data);
                window.location.reload();
        })
        .catch(err => {
                console.log('error =>', err);
        });
    }
    // -3-


    // -4- Rejection
    const rejectRequest = (id) => {
        const url = `https://dev-team4-backend.herokuapp.com/api/v1/notification/innovation-manager/response/${id}`;

        axios({
            method: 'POST',
            url: url,
            data: JSON.stringify({
                isApproved: false,
                note: createForm.note
            }),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
                console.log('response for your rejection =>', response.data.data);
                window.location.reload();
        })
        .catch(err => {
                console.log('error =>', err);
        });
    }
    // -4-


    const [indexListId, setIndexListId] = useState();


    // MODAL APPROVE //
    const {
        // buttonLabel,
        className
      } = props;
    
    const [modal, setModal] = useState(false);
    
    const toggle = (id) => {
        setModal(!modal);
        setIndexListId(id);
    }


    // MODAL REJECT //
    const [modal1, setModal1] = useState(false);

    const toggle1 = (id) => {
        setModal1(!modal1);
        setIndexListId(id);
    }


    // -5- Load More Function for Other Notification
    const loadMoreRequest = () => {
        setRequestCount(requestData.length);
    }
    // -5-

    
    // -6- Load More Function for Other Notification
    const loadMoreActivity = () => {
        setActivityCount(activityData.length);
    }
    // -6-


    // -7- Delete Other Notifications
    const delOtherNotification = (id) => {
        const url = `https://dev-team4-backend.herokuapp.com/api/v1/notification/activity/${id}`;

        axios({
            method: 'DELETE',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
            }
        }).then((response) => {
                console.log('response for your other notification deletion =>', response.data.data);
                window.location.reload();
        })
        .catch(err => {
                console.log('error =>', err);
        });
    }
    // -7-


    return (
       <Container className="container-whole">
           <Row className="template">

{/* =============================================================================================
========================================   REQUEST  ==========================================
============================================================================================= */}

            <Col sm={{ size: 6, offset: 0.5}} >
                <Card>
                    <CardTitle style={{
                      
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC",
                        }}>
                       <p className="JudulNotif">Requests</p>
                    </CardTitle >


                    {/* //CARD BODY 1// */}

                    <CardBody  style={{
                        height: '100%', 
                        marginTop:'-20px', 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"}}>
                    
                    {/* ----- Display Requests Notification Begin Code ----- */}
                    {requestData.slice(0, requestCount).map((list, i) => (

                    <Row>
                            <Col sm={{ size: 10, offset: 0.3}}>
                                <Row >
                                    <Col sm={{size: 5, offset: 0}} md={{size: 2, offset: 0}} style={{verticalAlign:"middle"}}>
                                        <Badge style={{
                                            backgroundColor:'#F9CC2C', 
                                            fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"}}>
                                                {list.type}
                                        </Badge>
                                    </Col >

                                    <Col sm={{size: 10, offset: 0}} style={{
                                        marginTop:"4px",
                                        color:"#979797", 
                                        fontSize:"10px", 
                                        fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"}}>
                                        {/* <FontAwesomeIcon icon={faClock} style={{color:'#979797'}}/> &nbsp; */}

                                        {Date(list.createdAt)}
                                    </Col>
                                </Row>

                                <Row style={{marginBottom:'-13px'}}>
                                <p style={{
                                    color:"#002C46",
                                    fontSize:"12px", 
                                    fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                    }}>
                                    <span style={{fontWeight:'bold'}}>
                                    {list.message}
                                    </span>
                                </p> 
                                </Row>

                                {list.links.map((link, j) => (

                                    <Col key={j} sm={{ size: 3, offset: 0.5}} style={{float:'left'}}>
                                        <Button href={link.download} style={{
                                            height:'25px', 
                                            width:'90px',
                                            color:"#095571", 
                                            fontSize:'9px', 
                                            borderColor:'#C5D4EB',
                                            backgroundColor:"#C5D4EB", 
                                            borderRadius:'5px', 
                                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }}>

                                            <p style={{
                                                marginLeft:'-10px', 
                                                marginRight:'-10px',
                                                marginTop:'-2px',
                                                fontSize:'10px'
                                                }}>
                                                Attachment {j+1}  &nbsp; &nbsp;
                                                <img src={download} alt='download icon  ' style={{color:"white", width: "10px"}}/>
                                            </p>

                                        </Button>
                                    </Col>

                                ))}

                            </Col>


                            <Col style={{alignItems:'center', marginTop:'20px'}}>
                                <Col>
                                    {/* //APPROVAL BUTTON// */}
        
                                    <Button style={{
                                        backgroundColor:"#45A35D",
                                        borderColor:"#45A35D", 
                                        width:'25px', 
                                        height: '30px',
                                        float:'left',
                                        marginRight:'2px'
                                        }} 
                                        onClick={() => toggle(list.id)}>
                                        <FontAwesomeIcon icon={faCheck} style={{
                                        color:'white',
                                        marginLeft:'-7px', 
                                        marginBottom:'10px'
                                        }}/>
                                    </Button>

                                    {/* //APPROVAL MODAL// */}

                                    <Modal isOpen={modal} toggle={toggle} className={className}>
                                        <ModalHeader  style={{
                                            color:"#45A35D", 
                                            fontWeight:"bold",
                                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }} 
                                            toggle={toggle}>
                                                Approval
                                        </ModalHeader>

                                        <ModalBody style={{
                                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }}>
                                            Note
                                            <Input type="textarea" name="note" onChange={(e) => setFieldForm('note', e.target.value)} placeholder="Any note?" rows={3} style={{fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"}}/>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button style={{
                                                backgroundColor:"#095571",
                                                fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                                }} 
                                                onClick={toggle}>
                                                Cancel
                                            </Button>{' '}

                                            <Button style={{
                                                backgroundColor:"#45A35D", 
                                                fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                                 }} 
                                                onClick={approveRequest(indexListId)}>
                                                Approve
                                            </Button>

                                        </ModalFooter>
                                    </Modal>
                                </Col>
                                
                                <Col>
                                    {/* //REJECT BUTTON// */}
        
                                    <Button style={{
                                        backgroundColor:"#EA2400",
                                        borderColor:"#EA2400", 
                                        width:'25px', 
                                        height: '30px', 
                                        backgroundPosition:'center'
                                        }} 
                                        onClick={toggle1}>
                                         <FontAwesomeIcon icon={faTimes} style={{
                                             color:'white',
                                             marginLeft:'-5px', 
                                             marginBottom:'10px'
                                             }}/>
                                    </Button>

                                    {/* //REJECT MODAL// */}

                                    <Modal isOpen={modal1} toggle1={toggle1} className={className}>
                                        <ModalHeader  style={{
                                            color:"#EA2400", 
                                            backgroundColor:"red", 
                                            fontWeight:"bold",
                                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }} toggle={toggle1}>
                                            Reject
                                        </ModalHeader>
                                        
                                        <ModalBody style={{
                                            color:"#002C46", 
                                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }}>
                                            Note
                                            <Input type="textarea" placeholder="Any note?" rows={3}/>
                                        </ModalBody>
                                        <ModalFooter>
                                            <Button style={{
                                                backgroundColor:"#095571", 
                                                fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"}} 
                                                onClick={toggle1}>
                                                Cancel
                                            </Button>{' '}
                                            <Button style={{backgroundColor:"#EA2400"}} onClick={rejectRequest(indexListId)}>Reject</Button>
                                        </ModalFooter>
                                    </Modal> 
                                </Col>    
                            </Col>
                        </Row>

                    ))}
                    {/* ----- Display Requests Notification End Code ----- */}

                    </CardBody>

{/* ///////////////////////////////////////////////////////////////////////////////////////////*/}

                    {/* //LOAD MORE RESULTS// */}

                    <CardBody style={{
                        textAlign:'center', 
                        marginTop:'15px',
                        marginBottom:'15px' }}> 
                        <Button style={{
                            backgroundColor:'#EBEBEB', 
                            borderColor:'#EBEBEB', 
                            color:'#231F20', 
                            fontSize:'12px',
                            fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                            }} onClick={loadMoreRequest}>
                                Load More Results
                        </Button>{' '}
                    </CardBody>


                </Card>
            </Col>

{/* =============================================================================================
================================   OTHER NOTIFICATION  ======================================
============================================================================================= */}

           <Col sm={{ size: 6, offset: 0.5}}  style={{
               borderBottom: 'solid',
               borderWidth:'1px', 
               borderBlockColor:"#CCCCCC"
               }} >

                <Card >
                    <CardTitle style={{ 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"
                        }}>
                       <p className="JudulNotif">Other Notifications</p>
                    </CardTitle >


                    {/* //CARD BODY 1// */}

                    <CardBody style={{
                        height: '100%', 
                        marginTop:'-20px', 
                        borderBottom: 'solid',
                        borderWidth:'1px', 
                        borderBlockColor:"#CCCCCC"}}>
                    
                    {/* ----- Display Activity Notification Begin Code ----- */}
                    {activityData.slice(0, activityCount).map((list, k) => (

                        <Row>
                            <Col sm={{ size: 1, offset: 0.5}} xs={{ size: 2, offset:0 }}>
                                <Button style={{
                                    backgroundColor:"#C4C4C4", 
                                    borderColor:'#C4C4C4', 
                                    width:'25px', 
                                    height: '30px', 
                                    backgroundPosition:'center'
                                    }} onClick={() => delOtherNotification(list.id)} disabled>
                                         <FontAwesomeIcon icon={faTimes} style={{
                                             color:'white',
                                             marginLeft:'-5px', 
                                             marginBottom:'10px'
                                             }}/>
                                </Button>
                            </Col>

                            <Col sm={{ size: 11, offset: 0.3}} xs={{ size: 10, offset:0 }}>
                                <Row style={{marginLeft:'9px'}} >
                                    <Col sm={{size: 1, offset: 0}}  style={{
                                        verticalAlign:"middle", 
                                        float:'right'
                                        }}>
                                        <Badge style={{
                                            backgroundColor:'#E15C30', 
                                            fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                            }}>
                                                {list.type}
                                        </Badge>
                                    </Col >

                                    <Col sm={{size: 11, offset: 0}} xs={{ size: 11, offset: -3}}  style={{
                                        float:'right', 
                                        marginTop:"4px", 
                                        marginLeft:'-20px',
                                        color:"#979797", 
                                        fontSize:"10px", 
                                        fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                        }}>
                                        <p style={{float:'right'}}><FontAwesomeIcon icon={faClock} style={{color:'#979797'}}/> 
                                            &nbsp;
                                            {Date(list.createdAt)}
                                        </p>
                                    </Col>
                                </Row>

                                <Row style={{
                                    marginBottom:'-13px', 
                                    marginTop:'-10px', 
                                    marginLeft:'5px'
                                    }}>
                                    <p style={{
                                        color:"#002C46", 
                                        fontSize:"12px", 
                                        fontFamily:"r-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
                                        }}>

                                        <span style={{fontWeight:'bold'}}>
                                            {list.message}
                                        </span>
                                    </p> 
                                </Row>
                            </Col>
                        </Row>
                    
                    ))}
                    {/* ----- Display Activity Notification End Code ----- */}

                    </CardBody>

                    <CardBody style={{textAlign:'center', marginTop:'15px', marginBottom:'15px' }}> 
                        <Button style={{backgroundColor:'#EBEBEB', borderColor:'#EBEBEB', color:'#231F20', fontSize:'12px',fontFamily:"-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"}} onClick={loadMoreActivity} >Load More Results</Button>{' '}
                    </CardBody>
                    
                </Card>
            </Col>     
            
           </Row>

       </Container>
    );

}

export default NotificationPage;