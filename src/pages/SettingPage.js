import React, {useState, useEffect} from 'react';
import "../assets/css/settingpage.css";
import { 
    TabContent,
    Container, 
    CardBody, 
    TabPane, 
    Nav, 
    NavItem, 
    NavLink, 
    Card, 
    Button, 
    FormGroup, 
    Label, 
    Input, 
    CardText, 
    Row, 
    Col,
    CardHeader, 
    Modal,
    ModalBody,
    ModalHeader,
} from 'reactstrap';
import classnames from 'classnames';
// import Select from "react-select";
import profilePicture from '../assets/profilepicture.svg';
import iconInfo from '../assets/icons/iconInfo.svg';
import iconPassword from '../assets/icons/iconPassword.svg';
import missionComplete from '../assets/missionComplete.svg';
import uploadPP from '../assets/icons/iconUploadProfPic.svg';
import axios from 'axios';
import { NotificationManager} from 'react-notifications';


const SettingPage = (props) => {
    const {
        buttonLabel,
        className
    } = props;


    // -1- Get Profile Data
    const [profileData, setProfileData] = useState();

    useEffect(() => {
        const fetchProfileData = async () => {
            axios({
                method: 'GET',
                    url: 'https://dev-team4-backend.herokuapp.com/api/v1/user/profile',
                    headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                    }
            }).then((response) => {
                console.log('response for profile data =>', response.data.data);
                setProfileData(response.data.data);
            }).catch(err => {
                console.log('error =>', err);
            });
        }
        fetchProfileData();
    }, []);
    // -1-


    // -2- Change Profile Data
    const [changeProfileData, setChangeProfileData] = useState({
        name: '',
        role: '',
        email: '',
        contact: '',
    });

    const handleChangeProfileData = (e) => {
        setChangeProfileData({
            ...changeProfileData,
            [e.target.name] : e.target.value
        })
    }

    // console.log('changeProfileData', changeProfileData);

    const submitChangeProfileData = () => {
        axios({
            method: 'PUT',
                url: 'https://dev-team4-backend.herokuapp.com/api/v1/user/profile',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                },
                data: JSON.stringify({
                    name: changeProfileData.name,
                    role: changeProfileData.role,
                    email: changeProfileData.email,
                    contact: changeProfileData.contact,
                }),
        }).then((response) => {
            console.log('response for profile data =>', response.data.data);
            setProfileData(response.data.data);
            toggleModalSaveData();
        }).catch((err) => {
            console.log('error =>', err);
            NotificationManager.error(`Oops! You haven't made any data changes!`,'',3000);
        });
    }
    // -2-


    // -3- Change Password Data
    const [changePassData, setChangePassData] = useState({
        currentPassword: '',
        newPassword: '',
        confirmPassword: '',
    });

    const handleChangePassData = (e) => {
        setChangePassData({
            ...changePassData,
            [e.target.name] : e.target.value
        })
    }

    // console.log('changePassData', changePassData);

    const submitChangePassData = () => {

        if (!changePassData.newPassword || !changePassData.confirmPassword) {
            NotificationManager.error(`Oops! You haven't write or confirm new password!`,'',3000);
        }
        else if (changePassData.newPassword !== changePassData.confirmPassword) {
            NotificationManager.error(`Oops! You write invalid password!`,'',3000);
        }
        else {
            axios({
                method: 'PUT',
                    url: 'https://dev-team4-backend.herokuapp.com/api/v1/user/change-password',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer '+localStorage.getItem("keepToken"),
                    },
                    data: JSON.stringify({
                        currentPassword: changePassData.currentPassword,
                        newPassword: changePassData.confirmPassword,
                    }),
            }).then((response) => {
                console.log('response for password data =>', response.data.data);
                setChangePassData(response.data.data);
                toggleModalChangePassword();
            }).catch((err) => {
                console.log('error =>', err);
                NotificationManager.error(`Oops! You haven't made any data changes!`,'',3000);

            });
        }
    }
    // -3-


    // MODAL FUNCTION FOR SAVING CHANGES//
    const [modalSaveData, setModalSaveData] = useState(false);
    const toggleModalSaveData = () => setModalSaveData(!modalSaveData);
    

    // MODAL FUNCTION FOR CHANGING PASSWORD//
    const [modalChangePassword, setModalChangePassword] = useState(false);
    const toggleModalChangePassword = () => setModalChangePassword(!modalChangePassword);


    // STATE FOR TAB PANE //
    const [activeTab, setActiveTab] = useState('1');
    const toggleTab = tab => {
    if(activeTab !== tab) setActiveTab(tab);
    }


    // EDIT PROFILE IMAGE //
    const [image, setImage] = useState({ preview: "", raw: "" });
    const handleChangeImage = e => {
      if (e.target.files.length) {
        setImage({
        preview: URL.createObjectURL(e.target.files[0]),
        raw: e.target.files[0]
        });
      }
    };
  
    // const handleUploadImage = async e => {
    //   e.preventDefault();
    //   const formData = new FormData();
    //   formData.append("image", image.raw);
  
    //   await fetch("YOUR_URL", {
    //     method: "POST",
    //     headers: {
    //       "Content-Type": "multipart/form-data"
    //     },
    //     body: formData
    //   });
    // };

    // await fetch("YOUR_URL", {
    //     method: "POST",
    //     headers: {
    //     "Content-Type": "multipart/form-data"
    //     },
    //     body: formData
    // });
    // };

    return (
        
        <Container fluid className="container-whole"> 

        {/* CARD 1 FOR USER DETAILS */}
            <Row className="template">
                <Col sm={{ size: 4, offset: 0}} className="col-spacing">
                    <Card style={{height: "100%"}}> 
                        <CardBody className="card-setting">
                            <Container>
                            <Row className="centerprofpic">
                                <img className="profile-picture" src={profilePicture} alt="profilePicture"/>
                            </Row>
                            <CardText className="profile-name">{localStorage.getItem('keepName')}</CardText>
                            <CardText className="profile-user">{localStorage.getItem('keepRole')}</CardText>
                            <CardText className="profile-email">{localStorage.getItem('keepEmail')}</CardText>
                            </Container>

                            {/* TABS FOR CHANGING USER DATA */}
                            <CardText className="active-tabs">
                            <Nav vertical>
                                <NavItem >
                                <NavLink style={{
                                    color: "#002C46", 
                                    padding: "2% 15% 2% 15%", 
                                    fontSize: "14px"}}
                                className={classnames({ active: activeTab === '1' })}
                                onClick={() => { toggleTab('1'); }}>
                                    <img style={{paddingRight: "20px"}}src={iconInfo} alt="personalInfo"/>
                                Personal Information
                                </NavLink>
                                </NavItem>
                                <NavItem >
                                <NavLink style={{
                                    color: "#002C46", 
                                    padding: "2% 15% 2% 15%", 
                                    fontSize: "14px"}}
                                className={classnames({ active: activeTab === '2' })}
                                onClick={() => { toggleTab('2'); }}>
                                    <img style={{paddingRight: "20px"}} src={iconPassword} alt="password"/>
                                Change Password
                                </NavLink>
                                </NavItem>
                            </Nav>
                            </CardText>  

                        </CardBody>
                    </Card>
                </Col>

               
                <Col sm={{ size: 8, offset: 0 }}>
                    <Card>
                        <CardBody >
                            <Container> 

                                {/* PERSONAL INFORMATION TAB CODES GO HERE */}
                            <TabContent activeTab={activeTab}>
                                <TabPane tabId="1">

                                <CardHeader style={{ background: 'white' }}>
                                        <CardText> 
                                            <Row>
                                                <Col sm={{size: 5}}>
                                                    <h3 className="card-heading-font1">Personal Information</h3>
                                                    <p className="card-heading-font2">Update your personal information</p>
                                                </Col>
                                                <Col sm={{size: 6, offset: 1}}>
                                                <Button style={{
                                                    backgroundColor:"#095571",  
                                                    color:"#FFFFFF", 
                                                    borderColor:"#095571", 
                                                    width: "100%", 
                                                    maxWidth: "120px",
                                                    marginRight: "10px", 
                                                    marginBottom: "10px", 
                                                    fontSize: "14px", 
                                                    marginTop: "10px"}} onClick={submitChangeProfileData}> Save changes </Button>
                                                <Button style={{
                                                    backgroundColor:"#FFFFFF",  
                                                    color:"#095571", 
                                                    borderColor:"#095571", 
                                                    width: "100%", 
                                                    maxWidth: "120px", 
                                                    fontSize: "14px"}} >Cancel{buttonLabel}</Button>
                                                </Col>
                                            </Row>
                                        </CardText>
                                    </CardHeader>


                                    <CardBody>
                                        <CardText className="card-body-title">User Information</CardText>
                                        <CardText style={{textAlign:"center"}}>
                                            <div>
                                                {image.preview ? (
                                                <img src={image.preview} alt="dummy" className="profile-picture3" />
                                                ) : (
                                                <>
                                                    <img className="profile-picture2" src={profilePicture} alt="profilePicture"/>
                                                </>
                                                )}
                                            </div>
                                            <div className="button-upload">    
                                                <label htmlFor="upload-button">
                                                    <img src={uploadPP} alt="buttonforupload" />
                                                </label>
                                                <input
                                                    type="file"
                                                    id="upload-button"
                                                    style={{ display: "none" }}
                                                    onChange={handleChangeImage}
                                                />
                                            </div>
                                        </CardText>
                                        <CardText className="file-type">Allow file type : jpg, jpeg</CardText>

                                        <FormGroup>
                                            <Label className="form-spacing" for="team">Name</Label>
                                            <Input
                                                name="name"
                                                className="form-spacing"
                                                placeholder={localStorage.getItem("keepName")}
                                                onChange={handleChangeProfileData}
                                                defaultValue={localStorage.getItem('keepName')}
                                            />
                                        </FormGroup>

                                        <FormGroup>
                                            <Label className="form-spacing" for="team">Role</Label>
                                            <Input 
                                                type="select" 
                                                placeholder="Pick your role"
                                                name="role"
                                                className="form-control"
                                                onChange={handleChangeProfileData}
                                                defaultValue={localStorage.getItem('keepRole')}>
                                                    <option selected disabled>Choose your role</option>
                                                    <option>Innovator</option>
                                                    <option>Innovation Manager</option>
                                            </Input>
                                        </FormGroup>

                                    </CardBody>


                                    <CardBody>
                                        <CardText><p className="card-body-title">Contact Information</p></CardText>
                                        <p className="card-heading-font2" >We'll share your email and phone number with anyone else</p>
                                    <FormGroup>
                                        <Label className="form-spacing" for="Email">Email Adress</Label>
                                        <Input 
                                            className="form-spacing"
                                            type="email" 
                                            name="email" 
                                            id="email" 
                                            placeholder={localStorage.getItem("keepEmail")} 
                                            onChange={handleChangeProfileData}
                                            defaultValue={localStorage.getItem('keepEmail')}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label className="form-spacing" for="contact">Contact Number </Label>
                                        <Input 
                                            className="form-spacing"
                                            type="text" 
                                            name="contact" 
                                            id="contact" 
                                            placeholder="Your contact number" 
                                            onChange={handleChangeProfileData}
                                        />
                                    </FormGroup>
                                    </CardBody>

                                </TabPane>


                              {/* PASSWORD CHANGE TAB CODES GO HERE */}
                                <TabPane tabId="2">

                                <CardHeader style={{ background: 'white' }}>

                                        <CardText style={{height: "60%"}}> 
                                            <Row>
                                                <Col sm={{size: 5}}>
                                                    <h3 className="card-heading-font1">Change Password</h3>
                                                    <p className="card-heading-font2">Change your account password</p>
                                                </Col>
                                                <Col sm={{size: 6, offset: 1}}>
                                                <Button style={{
                                                    backgroundColor:"#095571",  
                                                    color:"#FFFFFF", 
                                                    borderColor:"#095571", 
                                                    width: "100%", 
                                                    maxWidth: "120px", 
                                                    marginRight: "10px", 
                                                    marginBottom: "10px", 
                                                    marginTop: "10px",
                                                    fontSize: "14px"}} 
                                                    onClick={submitChangePassData}> Save changes</Button>
                                                <Button style={{
                                                    backgroundColor:"#FFFFFF",  
                                                    color:"#095571", 
                                                    borderColor:"#095571", 
                                                    width: "100%", 
                                                    maxWidth: "120px",
                                                    fontSize: "14px"}} >Cancel</Button>
                                                </Col>
                                            </Row>
                                        </CardText>
                                    </CardHeader>


                                    <CardBody style={{height: "520px"}}>
                                    <FormGroup>
                                        <Label className="form-spacing" for="currentPassword">Current Password</Label>
                                        <Input 
                                            className="form-spacing"
                                            type="password" 
                                            name="currentPassword" 
                                            id="currentPassword" 
                                            placeholder="Current Password" 
                                            onChange={handleChangePassData}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label className="form-spacing" for="newPassword">New Password </Label>
                                        <Input 
                                            className="form-spacing"
                                            type="password" 
                                            name="newPassword" 
                                            id="newPassword" 
                                            placeholder="New Password" 
                                            onChange={handleChangePassData}
                                        />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label className="form-spacing" for="confirmPassword">Confirm Password </Label>
                                        <Input 
                                            className="form-spacing"
                                            type="password" 
                                            name="confirmPassword" 
                                            id="confirmPassword" 
                                            placeholder="Confirm Password" 
                                            onChange={handleChangePassData}
                                        />
                                    </FormGroup>
                                    </CardBody>


                                </TabPane>
                            </TabContent>
                            </Container>                        
                        </CardBody>
                    </Card>


                    {/* MODAL FOR SAVE DATA*/}
                    <Modal isOpen={modalSaveData} 
                    toggle={toggleModalSaveData} 
                    className={className} 
                    style={{
                        textAlign:"center",
                        fontFamily: 'Roboto'}}>
                        <ModalHeader style={{
                            borderBlockColor:"white", 
                            height:"50px"}}
                            toggle={toggleModalSaveData}></ModalHeader>
                        <ModalBody style={{
                            textAlign: "center", 
                            height: "400px", 
                            paddingTop:"30px"}}>
                                <img src={missionComplete} alt='SaveData'/>
                        <ModalBody className="card-confirm2">Mission Complete</ModalBody>
                        <ModalBody className="card-note">Changes have been saved.</ModalBody>
                        <Button 
                        onClick={toggleModalSaveData} 
                        style={{
                            backgroundColor:"#095571", 
                            borderColor:"#095571", 
                            width: "100%", 
                            maxWidth: "160px", 
                            height:"48px", 
                            fontWeight: "bold"}} >Back to Setting</Button>{' '}
                        </ModalBody>
                    </Modal>

                    {/* MODAL FOR SAVE PASSWORD*/}
                    <Modal isOpen={modalChangePassword} 
                    toggle={toggleModalChangePassword} 
                    className={className} 
                    style={{
                        textAlign:"center",
                        fontFamily: 'Roboto'}}>
                        <ModalHeader style={{
                            borderBlockColor:"white", 
                            height:"50px"}}
                            toggle={toggleModalChangePassword}></ModalHeader>
                        <ModalBody style={{
                            textAlign: "center", 
                            height: "400px", 
                            paddingTop:"30px"}}>
                                <img src={missionComplete} alt='SaveData'/>
                        <ModalBody className="card-confirm2">Mission Complete</ModalBody>
                        <ModalBody className="card-note">Your password has been changed.</ModalBody>
                        <Button 
                        onClick={toggleModalChangePassword} 
                        style={{
                            backgroundColor:"#095571", 
                            borderColor:"#095571", 
                            width: "100%", 
                            maxWidth: "160px", 
                            height:"48px", 
                            fontWeight: "bold"}} >Back to Setting</Button>{' '}
                        </ModalBody>
                    </Modal>

                </Col>
            </Row>
        </Container>
    );

}

export default SettingPage;