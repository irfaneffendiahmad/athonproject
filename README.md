# Sprint.in - MONEV Scheduler
## Table of Contents
- [About Product](#about-product)
- [Project Team](#project-team)
- [Project Documents](#project-documents)
- [Product Deployment URL](#product-deployment-url-by-heroku)


## About Product
**Product Name** \
**Sprint.in - Monev Scheduler** \
An application devoted to help innovator in managing evaluation event schedule. This application was built in website by using React JS platform which has capability in mobile responsiveness.

**Product Goal** \
This application allows innovators to set their own agenda for monitoring and evaluation meeting (MONEV).

**Pain Points** \
To learn about the current process for monitoring and evaluation meeting, we interviewed 6 people to ask them about their preparation and activities in MONEV. The most prominent pain points are: 
1. Getting know innovation manager’s available schedule and book a meeting 
2. Making sure needed document for MONEV is ready 
3. Getting a notification for upcoming meeting 

**Solutions** \
We created a main solution to attempt to solve our users’ problems: 
> `Event page: This page allows user to book schedule for Monev with the necessity to upload needed documents as condition for approval` 

**Product Display** 
> <img src="./public/WebDisplay.png"> \
> <img src="./public/MobileDisplay.png"> \
> [Click to see menu in detail](/public/Sprint-inMenu.pdf)

[:house:](#table-of-contents) 


## Project Team
We collaborated as a team of 3 Front-end developers, 2 Back-end developers, 2 React Native developers, 2 UI/UX experts, dan 1 Product Manager to create Sprint.in. Our team is called as **TelkomAthon GROUP D**

**Product Manager**
1. Reza Trianto

**Front-end Developer Team**
1. Ahmad Irfan Effendi | [irfaneffendiahmad.gitlab.io](https://irfaneffendiahmad.gitlab.io) 
2. Yeni Purtika Simanjuntak | [gitlab/yenipurtika](https://gitlab.com/yenipurtika) 
3. Ariesta Mirania Fabiola | [gitlab/ariestamiraniaf](https://gitlab.com/ariestamiraniaf)

**Back-end Developer Team**
1. Rizaldy Pahlevi
2. Surya Daren Hafizh

**React Native Developer Team**
1. Ony Naraulita Maringga
2. Haris Altamira

**UI/UX Experts**
1. Tyassari Kusumaningsih
2. Dariant Deo

[:house:](#table-of-contents) 


## Project Documents
**UI/UX Protoype Page**: \
Figma: https://www.figma.com/file/KXqSzd1xbRCmiohu4epJY9/Final-Project-Tim-D \
User Flow Process: [UserFlowProcess.pdf](/public/UserFlowProcess.pdf)

**Front End Project Page**: \
https://gitlab.com/irfaneffendiahmad/athonproject \
https://git.digitalamoeba.id/irfaneffendiahmad/athonproject 

> **React JS src Folder Description** \
`assets`:  folder for images or any attached files which are used in app \
`components`: folder for additional functions JS files which are used in app such as content and sidebar \
`pages`: folder for pages files which are used in app \
`redux`: folder for redux files 

**Back End API Docs Page**: \
https://dev-team4-backend.herokuapp.com/api-docs

[:house:](#table-of-contents) 


## Product Deployment URL by Heroku
[athonproject.herokuapp.com](https://athonproject.herokuapp.com/) \
[sprint-in.herokuapp.com](https://sprint-in.herokuapp.com/)

[:house:](#table-of-contents) 

